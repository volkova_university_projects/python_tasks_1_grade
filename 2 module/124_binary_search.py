
def binary_search(input_list, search_term):
    start = 0
    end = len(input_list) - 1
    while start <= end:
        mid = (start + end) // 2
        guess = input_list[mid]
        if guess == search_term:
            return mid + 1
        if guess > search_term:
            end = mid - 1
        else:
            start = mid + 1
    return -1


n = int(input())  # количество элементов массива
x_raw = input().split()  # сырые элементы массива
x = list(map(int, x_raw)) if 1 <= len(x_raw) <= 10**5 else []  # приведённые к целым числам элементы массива
m = int(input())
m = m if 1 <= m <= 10**5 else 0 # число поисковых запросов
terms = [int(input()) for i in range(m)]
for term in terms:
    print(binary_search(x, term))  # ввод запроса и поиск с помощью бинарного поиска
