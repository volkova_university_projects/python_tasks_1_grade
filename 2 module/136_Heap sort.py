import sys


def heapify(array, number, i):
    largest = i
    left = 2 * i + 1
    right = 2 * i + 2

    if left < number and array[largest] < array[left]:
        largest = left

    if right < number and array[largest] < array[right]:
        largest = right

    if largest != i:
        array[i], array[largest] = array[largest], array[i]

        heapify(array, number, largest)



def heapSort(array):
    number = len(array)

    for i in range(number // 2 - 1, -1, -1):
        heapify(array, number, i)

    for i in range(number - 1, 0, -1):
        array[i], array[0] = array[0], array[i]  # swap
        heapify(array, i, 0)



element = int(input())
if element > 1000000:
    raise AssertionError
array = list(map(int, input().split()))
array = array[0:element]
heapSort(array)
n = len(array)

for i in range(n):
    sys.stdout.write(str(array[i]) + ' ')
sys.stdout.flush()



'''
6
6 4 7 1 9 -2
'''
