def merge(left, right):

    left_len = len(left)
    right_len = len(right)

    if left_len == 0:
        return right

    if right_len == 0:
        return left
    result = []
    index_left = index_right = 0

    while len(result) < left_len + right_len:

        if left[index_left] <= right[index_right]:
            result.append(left[index_left])
            index_left += 1
        else:
            result.append(right[index_right])
            index_right += 1

        if index_right == right_len:
            result += left[index_left:]
            break
        if index_left == left_len:
            result += right[index_right:]
            break
    return result


def merge_sort(array):
    array_len = len(array)
    if array_len < 2:
        return array
    midpoint = array_len // 2

    return merge(
        left=merge_sort(array[:midpoint]),
        right=merge_sort(array[midpoint:]))


enter_n = int(input())
array_raw = list(map(int, input().split()))
array = array_raw[0:enter_n]
n = len(array)
print(' '.join(map(str, merge_sort(array))))