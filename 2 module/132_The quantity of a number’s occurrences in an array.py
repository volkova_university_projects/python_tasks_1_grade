import sys


def first(arr, x, n):
    low = 0
    high = n - 1
    res = None

    while (low <= high):
        mid = (low + high) // 2

        if arr[mid] > x:
            high = mid - 1
        elif arr[mid] < x:
            low = mid + 1
        else:
            res = mid + 1
            high = mid - 1

    return res


def last(arr, x, n):
    low = 0
    high = n - 1
    res = None

    while (low <= high):

        mid = (low + high) // 2

        if arr[mid] > x:
            high = mid - 1
        elif arr[mid] < x:
            low = mid + 1
        else:
            res = mid + 1
            low = mid + 1

    return res


elem = int(input())
if elem > 100000:
    raise AssertionError
arr = list(map(int, input().split()))
arr = arr[0:elem]
n = len(arr)
search = int(input())
if search > 100000:
    raise AssertionError
answers = []
search_numbers = []
cache = {}

search_numbers = [int(sys.stdin.readline()) for line in range(search)]

'''
10
1 1 1 8 8 8 8 8 8 9
6
11
8
8
8
8
1
'''

for x in search_numbers:
    if x in cache:
        answers.append(cache[x])
        continue
    left = first(arr, x, n) or None
    right = last(arr, x, n) or None
    cache[x] = f'{right + 1 - left}' if left and right else 'Not found'
    answers.append(cache[x])
print('\n'.join(answers))