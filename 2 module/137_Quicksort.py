import random


def quicksort(array):
    if len(array) < 2:
        return array

    low, same, high = [], [], []

    pivot = array[random.randint(0, len(array) - 1)]

    for item in array:
        if item < pivot:
            low.append(item)
        elif item == pivot:
            same.append(item)
        elif item > pivot:
            high.append(item)

    return quicksort(low) + same + quicksort(high)

'''
9
9 -3 5 2 6 8 -6 1 3
'''
elem = int(input())
if elem > 0:
    array = list(map(int, input().split()))
    if len(array) > 1:
        print(' '.join(map(str, quicksort(array))))
    else:
        print(' '.join(map(str, array)))
else:
    print('')