import sys


class InteractiveArray:
    def __getitem__(self, item):
        return self._query_element(item)

    def _query_element(self, query):
        sys.stdout.write(f'? {query}\n')
        sys.stdout.flush()
        return int(sys.stdin.readline())


def first(x, n):
    low = 1
    high = n
    res = None
    array = InteractiveArray()

    while low <= high:
        mid = (low + high) // 2
        value = array[mid]

        if value > x:
            high = mid - 1
        elif value < x:
            low = mid + 1
        else:
            res = mid
            high = mid - 1

    return res - 1 if res else n


# def interactor_query(query):
#     sys.stdout.write(f'? {query}\n')
#     sys.stdout.flush()
#     return int(sys.stdin.readline())


def interactor_result(result):
    sys.stdout.write(f'! {result}\n')
    sys.stdout.flush()


n = int(input())
if n > 10 ** 9:
    raise AssertionError

interactor_result(n - first(1, n))

exit(0)

'''
def last(arr, x, n):
    low = 0
    high = n - 1
    res = None

    while low <= high:

        mid = (low + high) // 2

        if arr[mid] > x:
            high = mid - 1
        elif arr[mid] < x:
            low = mid + 1
        else:
            res = mid + 1
            low = mid + 1

    return res



'''
