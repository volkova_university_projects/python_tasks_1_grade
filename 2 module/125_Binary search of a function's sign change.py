# кубическое уравнение
def f(a, b, c, d, x):
    return a * x**3 + b * x**2 + c * x + d

# бинарный поиск корня


def binary_search(a, b, c, d):
    # проверка для отрицательных чисел
    if a < 0:
        a, b, c, d = -a, -b, -c, -d
    left = -1000
    right = 10000
    while left + 10**(-6) <= right:
        mid = (right + left) / 2
        if f(a, b, c, d, mid) < 0:
            left = mid
        else:
            right = mid
    return '{:.6f}'.format((left + right) / 2)


n = list(map(int, input().split()))
a, b, c, d = n[0], n[1], n[2], n[3]
print(binary_search(a, b, c, d))
