# look for the first digit of the number
def first_digit(num):
    return num[0]


# take any digit from the number of a given position
def get_any_digit(num, pos):
    return num[pos]


# convert a number to an array containing the given number
def number_to_array(num):
    return [num]


# function to check if the first number is smaller than the second
def first_is_smaller(num_1, num_2):
    len1 = number_of_digits(num_1)
    len2 = number_of_digits(num_2)
    diff = len1 - len2
    x = 0
    j = 0

    if diff == 0:
        return num_1 < num_2
    elif diff < 0:
        while x < len2:
            if j == len1:
                j = 0
            if get_any_digit(num_1, j) < get_any_digit(num_2, x):
                return True
            elif get_any_digit(num_1, j) > get_any_digit(num_2, x):
                return False
            x += 1
            j += 1
    else:
        while x < len1:
            if j == len2:
                j = 0
            if get_any_digit(num_2, j) < get_any_digit(num_1, x):
                return False
            if get_any_digit(num_2, j) > get_any_digit(num_1, x):
                return True
            x += 1
            j += 1

    return True


# fast recursive sorting
def quicksort(numbers):
    count = len(numbers)

    if count < 2:
        return numbers

    pivot = numbers[0]
    smaller = []
    bigger = []
    for elem in range(1, count):
        if (first_digit(numbers[elem]) == first_digit(pivot) and first_is_smaller(numbers[elem], pivot)) or \
                first_digit(numbers[elem]) < first_digit(pivot):
            smaller.append(numbers[elem])
        else:
            bigger.append(numbers[elem])
    return quicksort(bigger) + number_to_array(pivot) + quicksort(smaller)


# helper function that calculates the number of digits in a number
def number_of_digits(num):
    return len(num)


if __name__ == '__main__':
    array = []
    parts = int(input())

    for i in range(parts):
        number = input()
        if int(number) >= 0:
            array.append(number)
    array_2 = ''.join(quicksort(array))
    print(array_2)
