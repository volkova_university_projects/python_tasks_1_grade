import sys

def find(L, target):
    start = 0
    end = len(L) - 1
    while start <= end:
        middle = (start + end)// 2
        midpoint = L[middle]
        if midpoint > target:
            end = middle - 1
        elif midpoint < target:
            start = middle + 1
        else:
            return middle
    return -2

limit, strings_raw = int(input()), input().split()
strings = strings_raw[:limit]
count = int(input())
result = []
cache = {}
queries = [sys.stdin.readline().strip() for line in range(count)]

for search in queries:
    if search in cache:
        result.append(cache[search])
    else:
        result.append(find(strings, search) + 1)

len_result = len(result)

for i in range(len_result):
    sys.stdout.write(str(result[i]) + '\n')
sys.stdout.flush()

'''
9
BT EWC HSN IRDY QYUS TMQ U YOG ZE
4
ZE
EWC
ABC
ZE
'''