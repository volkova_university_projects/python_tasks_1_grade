
def binary_search(input_list, search_term):
    start = 0
    end = len(input_list) - 1
    while start <= end:
        mid = (start + end) // 2
        guess = input_list[mid]
        if guess == search_term:
            return mid + 1
        if guess > search_term:
            end = mid - 1
        else:
            start = mid + 1
    return -1