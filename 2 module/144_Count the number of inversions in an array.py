import sys
from copy import copy


def count_inversion(lst):
    return merge_count_inversion(lst)[1]


def merge_count_inversion(lst):
    if len(lst) <= 1:
        return lst, 0
    middle = int(len(lst) / 2)
    left, a = merge_count_inversion(lst[:middle])
    right, b = merge_count_inversion(lst[middle:])
    result, c = merge_count_split_inversion(left, right)
    return result, (a + b + c)


def merge_count_split_inversion(left, right):
    result = []
    count = 0
    i, j = 0, 0
    left_len = len(left)
    while i < left_len and j < len(right):
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            count += left_len - i
            j += 1
    result += left[i:]
    result += right[j:]
    return result, count


# MIN_MERGE = 32
#
#
# def calc_min_run(n):
#     r = 0
#     while n >= MIN_MERGE:
#         r |= n & 1
#         n >>= 1
#     return n + r
#
#
# def insertion_sort(arr, left, right):
#     for i in range(left + 1, right + 1):
#         j = i
#         while j > left and arr[j] < arr[j - 1]:
#             arr[j], arr[j - 1] = arr[j - 1], arr[j]
#             j -= 1
#
#
# def merge(arr, l, m, r):
#     len1, len2 = m - l + 1, r - m
#     left, right = [], []
#     for i in range(0, len1):
#         left.append(arr[l + i])
#     for i in range(0, len2):
#         right.append(arr[m + 1 + i])
#
#     i, j, k = 0, 0, l
#
#     while i < len1 and j < len2:
#         if left[i] <= right[j]:
#             arr[k] = left[i]
#             i += 1
#
#         else:
#             arr[k] = right[j]
#             j += 1
#
#         k += 1
#
#     while i < len1:
#         arr[k] = left[i]
#         k += 1
#         i += 1
#
#     while j < len2:
#         arr[k] = right[j]
#         k += 1
#         j += 1
#
#
# def lex_sort(arr):
#     n = len(arr)
#     min_run = calc_min_run(n)
#
#     for start in range(0, n, min_run):
#         end = min(start + min_run - 1, n - 1)
#         insertion_sort(arr, start, end)
#
#     size = min_run
#     while size < n:
#
#         for left in range(0, n, 2 * size):
#             mid = min(n - 1, left + size - 1)
#             right = min((left + 2 * size - 1), (n - 1))
#             if mid < right:
#                 merge(arr, left, mid, right)
#
#         size = 2 * size
def merge(left, right):

    left_len = len(left)
    right_len = len(right)

    if left_len == 0:
        return right

    if right_len == 0:
        return left
    result = []
    index_left = index_right = 0

    while len(result) < left_len + right_len:

        if left[index_left] <= right[index_right]:
            result.append(left[index_left])
            index_left += 1
        else:
            result.append(right[index_right])
            index_right += 1

        if index_right == right_len:
            result += left[index_left:]
            break
        if index_left == left_len:
            result += right[index_right:]
            break
    return result


def merge_sort(array):
    """
    9
    9 7 -1 3 0 -5 1 -1 3
    """
    array_len = len(array)
    if array_len < 2:
        return array
    midpoint = array_len // 2

    return merge(
        left=merge_sort(array[:midpoint]),
        right=merge_sort(array[midpoint:]))


def binary_count_inversions(a):
    # array_sorted = merge_sort(a)
    res = 0
    counts = [0] * (len(a) + 1)
    rank = {v: i + 1 for i, v in enumerate(sorted(a))}

    for x in reversed(a):
        i = rank[x] - 1
        while i:
            res += counts[i]
            i -= i & -i
        i = rank[x]
        while i <= len(a):
            counts[i] += 1
            i += i & -i
    return res


element = int(input())
array = list(map(int, sys.stdin.readline().strip().split()))
# array = [sys.stdin.readline().strip() for line in range(element)]
sys.stdout.write(str(binary_count_inversions(array)))
sys.stdout.flush()

'''
5
1 9 6 4 5
'''
