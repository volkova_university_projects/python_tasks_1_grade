

def binary_search(input_list, search_term):
    start = 0
    end = len(input_list) - 1
    while start <= end:
        mid = (start + end) // 2
        guess = input_list[mid]
        if guess == search_term:
            return mid + 1
        if guess > search_term:
            end = mid - 1
        else:
            start = mid + 1
    return -1

LN = list(map(int, input().split()))
L, N = LN[0], LN[1]
TV0 = []
print(L, N)
for i in range(N):
    TV = list(map(int, input().split()))
    # T, V = TV[0], TV[1]
    TV0.append(TV)
print(TV0)