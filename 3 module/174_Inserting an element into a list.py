class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self, data=""):
        self.first_node = None
        self.last_node = self.first_node
        for item in data.split():
            self.append(item)

    def append(self, val):
        if self.first_node is None:
            self.first_node = Node(val)
            self.last_node = self.first_node
            return
        self.last_node.next = Node(val)
        self.last_node = self.last_node.next

    def remove_item_by_value(self, value):
        current_node = self.first_node
        # previous_node = None

        while current_node is not None:
            if current_node.data == value:
                current_node.data = ''
                return
                # if previous_node is not None:
                #     previous_node.next = current_node.next
                # else:
                #     self.first_node = current_node.next

            # previous_node = current_node
            current_node = current_node.next

    def insert(self, value, idx):
        current_id = 1
        current_node = self.first_node
        if idx == 1:
            new_node = Node(value)
            new_node.next = self.first_node
            self.first_node = new_node
            return
        while current_node is not None:
            if current_id == idx - 1:
                new_node = Node(value)
                new_node.next = current_node.next
                current_node.next = new_node
                return
            current_node = current_node.next
            current_id += 1
        return

    def __repr__(self):
        temp_node = self.first_node
        result = ""
        while temp_node is not None:
            result += f"{temp_node.data} "
            temp_node = temp_node.next
        return result


string = input().split()
to_insert = string[-2]
idx = int(string[-1])

llist = LinkedList(' '.join(string[:-2]))
llist.insert(to_insert, idx)
print(llist)
