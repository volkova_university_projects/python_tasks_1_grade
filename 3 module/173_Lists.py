class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self, data):
        self.first_node = None
        self.last_node = self.first_node
        for item in data.split():
            self.append(item)

    def append(self, val):
        if self.first_node is None:
            self.first_node = Node(val)
            self.last_node = self.first_node
            return
        self.last_node.next = Node(val)
        self.last_node = self.last_node.next

    def display(self):
        temp_node = self.first_node
        while temp_node is not None:
            print(temp_node.data, end=' ')
            temp_node = temp_node.next
        print('\n')


string = input()

# string = string.split()
# print(string)
# string = "1 "*100000
# print(string)
llist = LinkedList(string)

llist.display()