

def knapsack(capacity, weights, quantity):
    if quantity == 0 or capacity == 0:
        return 0
    if weights[quantity - 1] > capacity:
        return knapsack(capacity, weights, quantity - 1)
    else:
        return max(knapsack(capacity - weights[quantity - 1], weights, quantity - 1)), \
               knapsack(capacity, weights, quantity - 1)

ingots_and_capacity = list(map(int, input().split()))
quantity, capacity = ingots_and_capacity[0], ingots_and_capacity[1]
# print(quantity, capacity)
if 1 <= quantity >= 1000:
    raise AssertionError
weights_raw = list(input().split())
weights = weights_raw[:len(weights_raw) - 1]
# print(weights)
print(knapsack(quantity, weights, quantity))