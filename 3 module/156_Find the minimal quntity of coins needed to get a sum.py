import sys


def min_coins(coins, amount):
    table = [0 for _ in range(amount + 1)]
    table[0] = 0

    for i in range(1, amount + 1):
        table[i] = sys.maxsize
        for coin in coins:
            if coin <= i:
                sub_result = table[i - coin]
                if sub_result != sys.maxsize and sub_result + 1 < table[i]:
                    table[i] = sub_result + 1

    if table[amount] == sys.maxsize:
        return -1

    return table[amount]


def to_list_of_ints(data):
    return list(map(int, data))


if __name__ == "__main__":
    num_coins, money = to_list_of_ints(input().split())
    coins = to_list_of_ints(input().split())[:num_coins]

    print(min_coins(coins, money))


exit(0)

'''
9 64
77 78 7 0 52 93 33 24 44
'''

# def min_number_of_coins(amount, coins):
#     if amount <= 0:
#         return 0
#     if amount in coins:
#         return 1
#     count = 0
#     for coin in sorted(coins, reverse=True):
#         if amount == 0:
#             break
#         if amount >= coin > 0:
#             count += amount // coin
#             amount = amount % coin
#         print(coin, count, amount)
#
#     return count


def min_number_of_coins(amount, coins):
    if amount <= 0:
        return 0
    if amount in coins:
        return 1
    for coin in sorted(coins, reverse=True):
        if amount >= coin > 0:
            print(amount, coin)
            return 1 + min_number_of_coins(amount - coin, coins)
    return 0


def to_list_of_ints(data):
    return list(map(int, data))


def min_coins(coins, num_coins, amount):
    if amount == 0:
        return 0

    result = 0
    for coin in coins:
        if 0 < coin <= amount:
            result = 1 + min_coins(coins, num_coins, amount - coin)

    return result


num_coins, money = to_list_of_ints(input().split())
coins = to_list_of_ints(input().split())[:num_coins]

print(min_coins(coins, num_coins, money))






#
# num_coins, money = to_list_of_ints(input().split())
# # print(num_coins, money)
#
# # if num_coins <= 0 or money <= 0:
# #     print(-1)
# #     exit()
# coins = to_list_of_ints(input().split())[:num_coins]

# print(change_making(coins, money))
'''
9 64 
77 78 7 0 52 93 33 24 44
'''