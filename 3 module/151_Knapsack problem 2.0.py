# def knapsack(capacity, weights, values, number):
#     if number == 0:
#         return 0
#     if weights[number - 1] > capacity:
#         return knapsack(capacity, weights, values, number - 1)
#     else:
#         return max(values[number - 1] + knapsack(capacity - weights[number - 1], weights, values, number - 1),
#                    knapsack(capacity, weights, values, number - 1))
def knapsack_dynamic(capacity, weights, values, num_values):
    result = [[0 for _ in range(capacity + 1)] for _ in range(num_values + 1)]
    print(result)

    for i in range(num_values + 1):
        for w in range(capacity + 1):
            if i == 0:
                result[i][w] = 0
            elif weights[i - 1] <= w:
                result[i][w] = max(values[i - 1] + result[i - 1][w - weights[i - 1]], result[i - 1][w])
                print(result[i][w])
            else:
                result[i][w] = result[i - 1][w]
                #print(result[i][w])
    return result[num_values][capacity]


if __name__ == '__main__':
    quantity, capacity = list(map(int, input().split()))

    if 1 <= quantity >= 1000:
        raise AssertionError

    value_raw = list(map(int, input().split()))
    weights_raw = list(map(int, input().split()))

    print(knapsack_dynamic(capacity, weights_raw, value_raw, len(weights_raw)))

exit(0)
'''
10 100
0 0 0 0 0 0 50 50 50 50
0 0 0 100 0 0 0 0 0 100
'''
'''
5 100
1000 550 550 550 550
80 50 50 50 50
'''
'''
5 100
1000 550 550 550 550
80 50 50 50 50
'''
'''
11 100
1000 550 0 9999 0 -1111 0 0 0 0 550
80 50 0 0 0 0 0 0 0 0 50
'''
'''
11 100
0 0 0 0 0 0 -1 0 0 0 0
0 0 0 0 0 0 100 0 0 0 0
'''
'''
5 10
-1 -1 -1 -1 -1
-1 -1 0 0 0
'''
