def check_balance(in_string):
    stack = []
    brackets_open = ["[", "{", "(", "<"]
    brackets_close = ["]", "}", ")", ">"]
    for i in range(len(in_string)):
        element = in_string[i]
        i += 1
        if element in brackets_open:
            stack.append((element, i))
        elif element in brackets_close:
            position = brackets_close.index(element)
            if len(stack) == 0 or (len(stack) > 0 and brackets_open[position] != stack[len(stack) - 1][0]):
                return 0
            elif len(stack) > 0 and brackets_open[position] == stack[len(stack) - 1][0]:
                stack.pop()
                continue
    return 1 if len(stack) == 0 else 0


if __name__ == '__main__':
    my_string = input()
    print(check_balance(my_string))