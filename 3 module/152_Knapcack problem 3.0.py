def knapsack_dynamic(capacity, weights, values, num_values):
    table = [[0 for _ in range(capacity + 1)] for _ in range(num_values + 1)]

    for i in range(num_values + 1):
        for w in range(capacity + 1):
            if i == 0:
                table[i][w] = 0
            elif weights[i - 1] <= w:
                table[i][w] = max(values[i - 1] + table[i - 1][w - weights[i - 1]], table[i - 1][w])
            else:
                table[i][w] = table[i - 1][w]
    result = table[num_values][capacity]

    indices = []

    for i in range(num_values, 0, -1):
        if result <= 0:
            break
        if result == table[i - 1][capacity]:
            continue
        else:
            indices.append(i)
            result = result - values[i - 1]
            capacity = capacity - weights[i - 1]
    indices = indices[::-1]
    return indices


if __name__ == '__main__':
    quantity, capacity = list(map(int, input().split()))

    if 1 <= quantity >= 1000:
        raise AssertionError

    value = list(map(int, input().split()))
    weights = list(map(int, input().split()))

    print(*knapsack_dynamic(capacity, weights, value, quantity))

'''
5 100
1000 550 550 550 550
80 50 50 50 50
'''

'''
2 10
100 80
10 9
'''
