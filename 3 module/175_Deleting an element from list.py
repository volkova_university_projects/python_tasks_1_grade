

class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self, data=""):
        self.first_node = None
        self.last_node = self.first_node
        for item in data.split():
            self.append(item)

    def append(self, val):
        if self.first_node is None:
            self.first_node = Node(val)
            self.last_node = self.first_node
            return
        self.last_node.next = Node(val)
        self.last_node = self.last_node.next

    def remove_item_by_value(self, value):
        current_node = self.first_node
        previous_node = None

        while current_node is not None:
            if current_node.data == value:
                if previous_node is not None:
                    previous_node.next = current_node.next
                else:
                    self.first_node = current_node.next

                del current_node
                return

            previous_node = current_node
            current_node = current_node.next

    def __repr__(self):
        temp_node = self.first_node
        result = ""
        while temp_node is not None:
            result += f"{temp_node.data} "
            temp_node = temp_node.next
        return result


string = input().split()
to_delete = string[-1]


llist = LinkedList(' '.join(string[:-1]))


llist.remove_item_by_value(to_delete)
print(llist)

'''
7 9 1 2 3 8 9
'''

