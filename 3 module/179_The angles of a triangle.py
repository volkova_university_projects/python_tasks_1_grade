import math


class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __repr__(self):
        return self.x, self.y

    def __str__(self):
        return str(" ".join(map(str, self.__repr__())))


class TriangleByPoints:
    def __init__(self, a=None, b=None, c=None):
        self.a = a
        self.b = b
        self.c = c
        self.len_ab = distance_between(self.a, self.b)
        self.len_bc = distance_between(self.b, self.c)
        self.len_ac = distance_between(self.a, self.c)

    def angle(self, a, b, c):
        return math.acos((a ** 2 + b ** 2 - c ** 2) / (2 * a * b)) * 180 / math.pi


def distance_between(point, target):
    dist = math.sqrt((point.x - target.x) ** 2 + (point.y - target.y) ** 2)
    return dist


def to_list_of_ints(data):
    return list(map(int, data))


def get_points_coords():
    a = Point(*to_list_of_ints(input().split()))
    b = Point(*to_list_of_ints(input().split()))
    c = Point(*to_list_of_ints(input().split()))
    return a, b, c


triangle = TriangleByPoints(*get_points_coords())
ordered_sides = sorted((triangle.len_ab, triangle.len_bc, triangle.len_ac))
result = triangle.angle(*ordered_sides)
print(f'{result:.6f}')


'''
0 0
3 0
0 4 
'''