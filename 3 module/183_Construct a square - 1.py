class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.x} {self.y}'


def findVertices(a, b):
    c = Point()
    d = Point()
    dx = b.x - a.x
    dy = b.y - a.y
    c.x = b.x - dy
    c.y = b.y + dx
    d.x = a.x - dy
    d.y = a.y + dx
    return f'{c} {d}'


def to_list_of_ints(data):
    return list(map(int, data))


if __name__ == '__main__':
    a = Point(*to_list_of_ints(input().split()))
    b = Point(*to_list_of_ints(input().split()))

    print(findVertices(a, b))