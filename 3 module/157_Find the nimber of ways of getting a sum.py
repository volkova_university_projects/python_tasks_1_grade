def get_ways(money, coins):
    ways = [0 for _ in range(money + 1)]
    ways[0] = 1 if money > 0 else 0
    for coin in coins:
        if coin <= 0:
            continue
        for element in range(len(ways)):
            if coin <= element:
                ways[element] += ways[element - coin]
    return ways[money] % (10**9 + 7)


def to_list_of_ints(data):
    return list(map(int, data))


num_coins, money = to_list_of_ints(input().split())
# print(num_coins, money)

# if num_coins <= 0 or money <= 0:
#     print(-1)
#     exit()
coins = to_list_of_ints(input().split())[:num_coins]

print(get_ways(money, coins))

'''
4 8
1 3 5 7 
'''
'''
0 0
0 0 0 0
'''
'''
0 -1
0 0 2 3 
'''

