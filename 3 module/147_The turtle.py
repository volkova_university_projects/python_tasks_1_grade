"""
3 4 
5 9 4 3 
3 1 6 9 
8 6 8 12
"""

height, width = [int(i) for i in input().split()]

grid = []
dp = [[None for _ in range(width)] for _ in range(height)]

for row in range(height):
    grid.append([int(i) for i in input().split()[:width]])

for row in enumerate(grid):
    i = row[0]
    for column in enumerate(row[1]):
        j = column[0]
        dp[i][j] = column[1]
        if i > 0 and j > 0:
            dp[i][j] += min(dp[i - 1][j], dp[i][j - 1])
        else:
            if i > 0:
                dp[i][j] += dp[i - 1][j]
            elif j > 0:
                dp[i][j] += dp[i][j - 1]

print(dp[-1][-1])