class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self, data=""):
        self.first_node = None
        self.last_node = self.first_node
        for item in data.split():
            self.append(item)

    def append(self, val):
        if self.first_node is None:
            self.first_node = Node(val)
            self.last_node = self.first_node
            return
        self.last_node.next = Node(val)
        self.last_node = self.last_node.next

    def _is_head(self, node):
        return node is self.first_node

    def _is_tail(self, node):
        return node is self.last_node

    def swap_two(self, first, second):
        first, f_prev = self.find_node_and_previous(first)
        second, s_prev = self.find_node_and_previous(second)
        # nothing to swap if any of args not found
        if not first and second:
            raise AssertionError
        # if any of elements is head or tail of the list - change link in container object
        self._change_head(first, second)
        self._change_tail(first, second)
        # swap links to objects
        if f_prev:
            f_prev.next = second
        if s_prev:
            s_prev.next = first
        # swap links in objects
        second.next, first.next = first.next, second.next

    def _change_head(self, first, second):
        if self._is_head(first):
            self.first_node = second
        elif self._is_head(second):
            self.first_node = first

    def _change_tail(self, first, second):
        if self._is_tail(first):
            self.last_node = second
        elif self._is_tail(second):
            self.last_node = first

    def find_node_and_previous(self, value):
        current_node = self.first_node
        previous_node = None
        while current_node is not None:
            if current_node.data == value:
                return current_node, previous_node
            previous_node = current_node
            current_node = current_node.next
        return None, None

    def remove_item_by_value(self, value):
        current_node = self.first_node
        previous_node = None

        while current_node is not None:
            if current_node.data == value:
                if previous_node is not None:
                    previous_node.next = current_node.next
                else:
                    self.first_node = current_node.next

                del current_node
                return

            previous_node = current_node
            current_node = current_node.next

    def insert(self, value, idx):
        current_id = 1
        current_node = self.first_node
        if idx == 1:
            new_node = Node(value)
            new_node.next = self.first_node
            self.first_node = new_node
            return
        while current_node is not None:
            if current_id == idx - 1:
                new_node = Node(value)
                new_node.next = current_node.next
                current_node.next = new_node
                return
            current_node = current_node.next
            current_id += 1
        return

    def __repr__(self):
        temp_node = self.first_node
        result = ""
        while temp_node is not None:
            result += f"{temp_node.data} "
            temp_node = temp_node.next
        return result


if __name__ == '__main__':
    string = input().split()
    first = string[-2]
    second = string[-1]
    llist = LinkedList(' '.join(string[:-2]))
    llist.swap_two(first, second)
    print(llist)

'''
7 9 1 2 3 8 1 2
'''