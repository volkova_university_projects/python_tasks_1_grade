class Tree:
    def __repr__(self):
        return self.traverse_postorder(self.root)

    def __init__(self, data=""):
        self.root = Node()
        self.last_leaf = self.root
        for item in data:
            self.insert(self.root, item)

    def insert(self, node=None, data=None):
        if node is None:
            return Node(data)
        if node.data:
            if data < node.data:
                node.left = self.insert(node.left, data)
            elif data > node.data:
                node.right = self.insert(node.right, data)
        else:
            node.data = data
        return node

    def traverse_inorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            self.traverse_inorder(root.left)
            print(root.data)
            self.traverse_inorder(root.right)

    def traverse_preorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            print(root.data)
            self.traverse_preorder(root.left)
            self.traverse_preorder(root.right)

    def traverse_postorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            self.traverse_postorder(root.left)
            self.traverse_postorder(root.right)
            print(root.data)

    def walk_tree(self, direction, node=None):
        node = self.root if node is None else node
        if direction == "left" and node.left:
            return node.left
        if direction == "right" and node.right:
            return node.right
        return False


class Node:
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None



tree_len = int(input())

# tree = Tree("27 14 35 10 19 31 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60")
tree = Tree(list(map(int, input().split())))
num_turns = int(input())
print(tree)

node = tree.root
for i in range(num_turns):
    temp_node = tree.walk_tree(input(), node)
    if not temp_node:
        print("empty")
    else:
        node = temp_node
        print(node.data)

'''
99 7 31 14 60 18 37 -1
'''


