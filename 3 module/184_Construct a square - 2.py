class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.x} {self.y}'


def findVertices(a, b):
    c = Point()
    d = Point()
    dx = b.x - a.x
    dy = b.y - a.y
    c.x = b.x - dy
    c.y = b.y + dx
    d.x = a.x - dy
    d.y = a.y + dx
    return f'{c} {d}'


def find(a, c):
    b = Point()
    d = Point()
    a = Point(a.x, a.y)
    c = Point(c.x, c.y)
    # The centroid of the square and intersection of the diagonals is (m1, m2) like as (x, y)
    m1 = 1/2 * (a.x + c.x)
    m2 = 1/2 * (a.y + c.y)
    # The vector of M to C is (mc1, mc2) like as (x, y)
    mc1 = c.x - m1
    mc2 = c.y - m2
    v1 = c.x - a.x
    v2 = c.y - a.x
    # b =
    # d =


def to_list_of_ints(data):
    return list(map(int, data))


if __name__ == '__main__':
    a = Point(*to_list_of_ints(input().split()))
    b = Point(*to_list_of_ints(input().split()))

    print(findVertices(a, b))