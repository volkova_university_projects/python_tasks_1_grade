
'''
5
3 -3 6 1 5

6
1 4 5 10 20 6
'''
# N = int(input())
# arr = list(map(int, input().split()))

water_lily = int(input())
if 0 < water_lily > 10**5:
    raise AssertionError
count_lily_raw = list(map(int, input().split()))
arr = count_lily_raw[0:water_lily]

ways = [(arr[0], [0]), (arr[1], [1])]

for i in range(2, water_lily):
    min_index = -1 if arr[i - 1] < arr[i - 2] else -2
    ways.append((arr[i] + ways[min_index][0], ways[min_index][1] + [i]))

print(ways[-1][0])
print(*ways[-1][1])


#
#
# print(count_water_lily[::2])
#
# print(count_water_lily[::3])


