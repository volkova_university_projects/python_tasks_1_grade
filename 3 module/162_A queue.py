class queue:
    def __init__(self):
        self.first_side = []
        self.second_side = []


    def en_queue(self, number):
        self.first_side.append(number)


    def de_queue(self):

        if len(self.first_side) == 0 and len(self.second_side) == 0:
            print('Queue is empty')
            return

        elif len(self.second_side) == 0 and len(self.first_side) > 0:
            while len(self.first_side):
                temp = self.first_side.pop()
                self.second_side.append(temp)
            return self.second_side.pop()
        else:
            return self.second_side.pop()


if __name__ == '__main__':
    q = queue()
    q.en_queue(1)
    q.en_queue(2)
    q.en_queue(3)

    print(q.de_queue())
    print(q.de_queue())
    print(q.de_queue())

# size_and_cash_desks = list(map(int, input().split()))
# size, cash_desks = size_and_cash_desks[0], size_and_cash_desks[1]
#
# clients_raw = list(map(int, input().split()))
# clients = clients_raw[:size]
# print(size, cash_desks, clients)