'''
https://towardsdatascience.com/solving-tsp-using-dynamic-programming-2c77da86610d
about
https://github.com/DalyaG/CodeSnippetsForPosterity/blob/master/SolvingTSPUsingDynamicProgramming/SolvingTSPUsingDynamicProgramming.ipynb
code
'''


def traveling_salesman(distances_array):
    n = len(distances_array)
    all_points_set = set(range(n))
    memo = {(tuple([i]), i): tuple([0, None]) for i in range(n)}
    queue = [(tuple([i]), i) for i in range(n)]

    while queue:
        prev_visited, prev_last_point = queue.pop(0)
        prev_dist, _ = memo[(prev_visited, prev_last_point)]

        to_visit = all_points_set.difference(set(prev_visited))
        for new_last_point in to_visit:
            new_visited = tuple(sorted(list(prev_visited) + [new_last_point]))
            new_dist = prev_dist + distances_array[prev_last_point][new_last_point]

            if (new_visited, new_last_point) not in memo:
                memo[(new_visited, new_last_point)] = (new_dist, prev_last_point)
                queue += [(new_visited, new_last_point)]
            else:
                if new_dist < memo[(new_visited, new_last_point)][0]:
                    memo[(new_visited, new_last_point)] = (new_dist, prev_last_point)

    optimal_path, optimal_cost = retrace_optimal_path(memo, n)

    return optimal_path, optimal_cost


def retrace_optimal_path(memo: dict, n: int) -> [[int], float]:
    points_to_retrace = tuple(range(n))

    full_path_memo = dict((k, v) for k, v in memo.items() if k[0] == points_to_retrace)
    path_key = min(full_path_memo.keys(), key=lambda x: full_path_memo[x][0])

    last_point = path_key[1]
    optimal_cost, next_to_last_point = memo[path_key]

    optimal_path = [last_point]
    points_to_retrace = tuple(sorted(set(points_to_retrace).difference({last_point})))

    while next_to_last_point is not None:
        last_point = next_to_last_point
        path_key = (points_to_retrace, last_point)
        _, next_to_last_point = memo[path_key]

        optimal_path = [last_point] + optimal_path
        points_to_retrace = tuple(sorted(set(points_to_retrace).difference({last_point})))

    return optimal_path, optimal_cost


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def increment(arg):
    return arg + 1


if __name__ == '__main__':
    vertex_count = int(input())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    optimal_path, path_cost = traveling_salesman(adj_matrix)
    print(path_cost)
    print(*list(map(increment, optimal_path)))



'''




from sys import maxsize
from itertools import permutations

# def permutations(iterable, r=None):
#     # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
#     # permutations(range(3)) --> 012 021 102 120 201 210
#     pool = tuple(iterable)
#     n = len(pool)
#     r = n if r is None else r
#     if r > n:
#         return
#     indices = list(range(n))
#     cycles = list(range(n, n-r, -1))
#     yield tuple(pool[i] for i in indices[:r])
#     while n:
#         for i in reversed(range(r)):
#             cycles[i] -= 1
#             if cycles[i] == 0:
#                 indices[i:] = indices[i+1:] + indices[i:i+1]
#                 cycles[i] = n - i
#             else:
#                 j = cycles[i]
#                 indices[i], indices[-j] = indices[-j], indices[i]
#                 yield tuple(pool[i] for i in indices[:r])
#                 break
#         else:
#             return


def travellingSalesmanProblem(graph, s, vertex_count):
    vertex = []
    for i in range(vertex_count):
        if i != s:
            vertex.append(i)

    min_path = maxsize
    next_permutation = permutations(vertex)
    for i in next_permutation:

        current_pathweight = 0

        k = s
        for j in i:
            current_pathweight += graph[k][j]
            k = j
        current_pathweight += graph[k][s]

        min_path = min(min_path, current_pathweight)

    return min_path


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == "__main__":
    # graph = [[0, 10, 15, 20], [10, 0, 35, 25],
    #          [15, 35, 0, 30], [20, 25, 30, 0]]
    vertex_count = int(input())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    s = 0
    print(travellingSalesmanProblem(adj_matrix, s, vertex_count))

'''
'''
5
0 5 3 2 4
4 0 5 4 1
3 6 0 4 2
2 4 4 0 5
6 1 2 7 0
'''


'''
12
0 5 3 2 4 1 5 3 2 4 2 1 
4 0 5 4 1 4 8 5 4 1 2 1
3 6 0 4 2 3 6 1 4 2 2 1
2 4 4 0 5 2 4 4 2 5 2 1
6 1 2 7 0 6 1 2 7 1 2 1 
1 5 3 2 4 0 5 3 2 4 2 1 
4 2 5 4 1 4 0 5 4 1 2 1
3 6 1 4 2 3 6 0 4 2 2 1
2 4 4 1 5 2 4 4 0 5 2 1
6 1 2 7 1 6 1 2 7 0 2 1
3 6 2 4 2 3 6 1 4 2 0 1
2 4 4 1 5 2 4 4 1 5 2 0 
'''
#1 2 3 2
'''
4
0 10 15 20
10 0 35 25
15 35 0 30
20 25 30 0
'''

