import sys

def convert_to_edge_list(matrix):
    result = []
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value == 1 and i != j:
                result.append(list_to_str(f'{i + 1} {j + 1}'))
                matrix[j][i] = 0
    return result


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count = int(input())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
adj_list = convert_to_edge_list(adj_matrix)
print(adj_list)
# for item in adj_list:
#     sys.stdout.write(f'{item}\r\n')
# sys.stdout.flush()

""" 
5
0 0 1 0 0
0 0 1 0 1
1 1 0 0 0
0 0 0 0 0
0 1 0 0 0
"""

""" 
5
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0
"""

""" 
5
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
"""
'''
4
0 1 1 0
1 0 0 0
1 0 0 0
0 0 0 0
'''

'''
3
0 1 1
4 0 1
2 1 0
'''


