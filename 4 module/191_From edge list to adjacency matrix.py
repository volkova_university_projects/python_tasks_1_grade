
vertexes, edges = map(int, input().split())
adj = [[0] * vertexes for _ in range(vertexes)]

for item in range(edges):
    edge, vertex, weight = map(int, input().split())
    adj[edge - 1][vertex - 1] = adj[vertex - 1][edge - 1] = weight

for element in adj:
    print(" ".join(map(str, element)))



'''
5 3 
1 3 
2 3 
2 5 
'''

'''
7 6
3 1 4
0 2 13
0 3 19
1 4 15
6 5 10
4 6 8
'''

'''
4 8
1 2 723
2 1 723
2 3 137
2 4 470
3 2 137
3 4 69
4 2 470
4 3 69
'''