def convert_to_edge_list(matrix):
    result = []
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value == 1 and i != j:
                result.append([i + 1, j + 1])
                matrix[j][i] = 0
    return result


def next_node(edge, current):
    return edge[0] if current == edge[1] else edge[1]


def remove_edge(raw_list, discard):
    return [item for item in raw_list if item != discard]


def euler_tour(graph):
    search = [[[], graph[0][0], graph]]
    while search:
        path, node, unexplored = search.pop()
        path += [node]

        if not unexplored:
            return path

        for edge in unexplored:
            if node in edge:
                search += [[path, next_node(edge, node), remove_edge(unexplored, edge)]]


def to_list_of_ints(data):
    return list(map(int, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    vertex_count = int(input())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    adj_list = convert_to_edge_list(adj_matrix)
    print(*euler_tour(adj_list))

'''
5
0 1 1 1 1
1 0 1 1 1
1 1 0 1 1
1 1 1 0 1
1 1 1 1 0
'''
