def convert_to_adj_list(matrix):
    result = {}
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value == 1:
                if result.get(i + 1):
                    result[i + 1].append(j + 1)
                else:
                    result[i + 1] = [j + 1]
    return result


def hamilton(graph, start_v):
    size = len(graph)
    to_visit = [None, start_v]
    path = []
    while to_visit:
        v = to_visit.pop()
        if v:
            path.append(v)
            if len(path) == size:
                break
            for x in set(graph[v]) - set(path):
                to_visit.append(None)
                to_visit.append(x)
        else:
            path.pop()
    # path.append(start_v)
    return path


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count, start_vertex = map(int, input().split())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
adj_list = convert_to_adj_list(adj_matrix)
path = hamilton(adj_list, start_vertex)
print(*path)

'''
5 4
0 1 1 0 0
1 0 0 1 0
1 0 0 1 1
0 1 1 0 1
0 0 1 1 0
'''
