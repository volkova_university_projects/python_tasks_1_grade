
def initialise(vertex_count, graph):
    global dis
    for i in range(vertex_count):
        for j in range(vertex_count):
            dis[i][j] = graph[i][j] if graph[i][j] != -1 else float('inf')


def floyd_warshall(vertex_count):
    for k in range(vertex_count):
        for i in range(vertex_count):
            for j in range(vertex_count):
                if dis[i][k] == float('inf') or dis[k][j] == float('inf'):
                    continue
                if dis[i][j] > dis[i][k] + dis[k][j]:
                    dis[i][j] = dis[i][k] + dis[k][j]
    return dis


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    MAXM = 50
    dis = [[-1 for i in range(MAXM)] for i in range(MAXM)]

    vertex_count, start, end = to_list_of_ints(input().split())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)


    initialise(vertex_count, adj_matrix)
    result = floyd_warshall(vertex_count)
    print(result[start - 1][end - 1])

'''
3 1 2
0 -1 3
7 0 1
2 215 0
'''