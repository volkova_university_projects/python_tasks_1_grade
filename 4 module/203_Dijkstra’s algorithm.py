class Graph:

    def __init__(self):
        self.graph = {}

    def __getitem__(self, item):
        return self.graph[item]

    def from_adj_matrix(self, matrix):
        self.graph = dict({i: {} for i in range(1, len(matrix) + 1)})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if value >= 0 and elem != num:
                    self.graph[elem + 1][num + 1] = value


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


class Dijkstra:

    def __init__(self, vertices, graph):
        self.vertices = vertices
        self.graph = graph

    def find_route(self, start, end):
        unvisited = {n: float("inf") for n in self.vertices}
        unvisited[start] = 0
        visited = {}
        parents = {}
        while unvisited:
            min_vertex = min(unvisited, key=unvisited.get)
            for neighbour in self.graph.get(min_vertex, {}).keys():
                if neighbour in visited:
                    continue
                new_distance = unvisited[min_vertex] + self.graph[min_vertex].get(neighbour, float("inf"))
                if new_distance < unvisited[neighbour]:
                    unvisited[neighbour] = new_distance
                    parents[neighbour] = min_vertex
            visited[min_vertex] = unvisited[min_vertex]
            unvisited.pop(min_vertex)
            if min_vertex == end:
                break
        return parents, visited


vertex_count, start, end = to_list_of_ints(input().split())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)

input_graph = Graph()
input_graph.from_adj_matrix(adj_matrix)

dijkstra = Dijkstra(input_graph.graph.keys(), input_graph.graph)
p, v = dijkstra.find_route(start, end)
print(v.get(end) if v.get(end) != float("inf") else -1)

'''
3 2 1
0 1 1
4 0 1
2 1 0
'''