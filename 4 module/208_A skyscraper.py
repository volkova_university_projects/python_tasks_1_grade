import sys


class SkyscraperGraphFromEdges:

    def __init__(self, vertices, edges, default):
        self.default_weight = default
        self.graph = self._init_graph(vertices, default)
        self.edge = []
        self._from_edge_list(edges)
        self._fill_edges()

    def _init_graph(self, vertices, default):
        graph = dict({i: {i - 1: default if i > 1 else float('inf'),
                         i + 1: default if i != vertices else float('inf')}
                     for i in range(0, vertices + 1)})
        del(graph[vertices][vertices+1])
        del(graph[1][0])
        return graph

    def __getitem__(self, item):
        return self.graph[item]

    def _from_edge_list(self, edges):
        for src, dst, weight in edges:
            self.graph[src][dst] = min(weight, self.graph[src].get(dst, float('inf')))
            self.graph[dst][src] = min(weight, self.graph[dst].get(src, float('inf')))

    def _fill_edges(self):
        for src, inner in self.graph.items():
            for destination, weight in inner.items():
                if weight != float('inf'):
                    self.edge.append({'src': src, 'dest': destination, 'weight': weight})

    def __repr__(self):
        return f'{self.graph}'


class Dijkstra:

    def __init__(self, vertices, graph):
        self.vertices = vertices
        self.graph = graph

    def find_route(self, start, end):
        unvisited = {n: float("inf") for n in self.vertices}
        unvisited[start] = 0
        visited = {}
        parents = {}
        while unvisited:
            min_vertex = min(unvisited, key=unvisited.get)
            for neighbour in self.graph.get(min_vertex, {}).keys():
                if neighbour in visited:
                    continue
                new_distance = unvisited[min_vertex] + self.graph[min_vertex].get(neighbour, float("inf"))
                if new_distance < unvisited[neighbour]:
                    unvisited[neighbour] = new_distance
                    parents[neighbour] = min_vertex
            visited[min_vertex] = unvisited[min_vertex]
            unvisited.pop(min_vertex)
            if min_vertex == end:
                break
        return parents, visited


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(sys.stdin.readline().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    vertex_count, edge_count, default_weight = to_list_of_ints(input().split())
    start, end = to_list_of_ints(input().split())
    elevators = get_int_lines(edge_count)
    skyscraper = SkyscraperGraphFromEdges(vertex_count, elevators, default_weight)
    dijkstra = Dijkstra(skyscraper.graph.keys(), skyscraper.graph)
    _, cost = dijkstra.find_route(start, end)
    sys.stdout.write(str(cost[end]))


'''3
5 3 2
1 3
1 2 1
1 5 4
4 5 2
'''
'''
5 3 2
1 5
1 2 1
1 5 4
4 5 2
'''

'''
1000 100 2
1 3
74 63 10
44 18 -1
34 5 2
63 95 3
94 87 5
82 44 2
96 23 7
85 2 2
4 99 9
13 64 5
33 31 4
72 18 5
45 52 8
50 31 10
26 95 7
46 88 1
41 57 9
12 12 10
30 72 2
99 11 8
41 5 1
16 59 7
59 34 1
14 40 5
40 92 6
51 41 3
71 3 4
97 97 10
18 25 6
81 93 7
12 68 2
27 96 6
72 65 10
63 2 1
41 41 1
99 53 1
56 30 6
76 58 10
99 81 3
92 96 4
30 100 9
78 22 5
85 98 4
51 88 7
82 61 8
97 93 10
99 12 4
6 28 5
3 42 4
97 95 6
50 61 10
70 72 3
77 92 8
8 93 7
86 3 2
76 34 6
34 3 2
94 21 2
31 79 6
37 90 8
63 39 3
92 6 6
31 69 1
50 91 5
16 1 8
44 99 6
41 5 1
90 80 8
29 17 5
38 3 10
22 31 4
72 83 1
2 37 2
63 79 8
9 34 1
17 90 2
7 51 5
14 70 2
43 79 5
34 82 7
5 85 7
45 22 10
57 61 9
80 42 3
96 60 5
84 75 5
68 23 9
36 93 1
3 56 10
44 15 1
35 97 1
16 68 5
22 4 9
56 63 9
69 9 8
4 63 2
75 75 -1
16 52 7
79 15 8
52 12 8

'''