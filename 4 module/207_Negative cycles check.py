import sys

class WeightedGraph:

    def __init__(self, matrix):
        self.graph = {}
        self.edge = []
        self._from_adj_matrix(matrix)
        self._fill_edges()

    def __repr__(self):
        return f'{self.graph}'

    def __getitem__(self, item):
        return self.graph[item]

    def _from_adj_matrix(self, matrix):
        self.graph = dict({i: {} for i in range(len(matrix))})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if abs(value) < 100000:
                    self.graph[elem][num] = value

    def _fill_edges(self):
        for src, inner in self.graph.items():
            for destination, weight in inner.items():
                self.edge.append(Edge(src, destination, weight))


class Edge:
    def __init__(self, src=None, dest=None, weight=None):
        self.src = src
        self.dest = dest
        self.weight = weight

    def __repr__(self):
        return f"Edge({self.src}, {self.dest}, {self.weight})"


def neg_cycle_bellman_ford(graph, src):
    V = len(graph.graph)

    dist = [sys.maxsize for _ in range(V)]
    parent = [-1 for _ in range(V)]
    dist[src] = 0

    for _ in range(1, V):
        for edge in graph.edge:
            u = edge.src
            v = edge.dest
            weight = edge.weight

            if (dist[u] != 1000000 and
                    dist[u] + weight < dist[v]):
                dist[v] = dist[u] + weight
                parent[v] = u

    C = -1
    for edge in graph.edge:
        u = edge.src
        v = edge.dest
        weight = edge.weight

        if (dist[u] != 1000000 and
                dist[u] + weight < dist[v]):
            C = v
            break

    if C == -1:
        return None
    for _ in range(V):
        C = parent[C]

    cycle = []
    v = C

    while True:
        cycle.append(v)
        if v == C and len(cycle) > 1:
            break
        v = parent[v]

    cycle.reverse()

    return cycle if len(cycle) > 2 else list(map(increment, cycle))


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def increment(number):
    return number + 1


def make_output(result):
    if not result:
        return 'NO'
    else:
        return f"YES\n" \
               f"{len(result)}\n" \
               f"{list_to_str(map(increment, result), separator=' ')}"


if __name__ == '__main__':
    vertex_count = int(input())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    graph = WeightedGraph(adj_matrix)
    print(graph)
    result = neg_cycle_bellman_ford(graph, 0)
    print(make_output(result))

'''
3
100000 100000 -100
100 100000 100000
100000 -50 100000
'''

'''
3
100000 100000 -31
100 100000 100000
100000 -30 100000
'''

'''
5
-100 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1

'''