class Graph:
    def __init__(self, edge_list, num_of_nodes):
        self.adjacency_list = {vertex + 1: [] for vertex in range(num_of_nodes)}
        for (origin, dest) in edge_list:
            self.adjacency_list[origin].append(dest)
            self.adjacency_list[dest].append(origin)

    def __repr__(self):
        return list_to_str(
            [f'{vertex}: {list_to_str(adj_vertices, separator=" ")}'
             for vertex, adj_vertices in self.adjacency_list.items()],
            separator="\n")


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count, edge_count = to_list_of_ints(input().split())
edge_list = get_int_lines(edge_count, row_limit=2)
graph = Graph(edge_list, vertex_count)
print(graph)
"""
5 7
1 2
1 3
1 4
2 4
2 5
3 4
4 5
"""

# if __name__ == "__main__":
#     # Set up an edge list and number of nodes
#     edge_list = [(0, 1), (1, 2), (2, 3), (0, 2), (3, 2), (4, 5), (5, 4)]
#     num_of_nodes = 6
#
#     graph = Graph(edge_list, num_of_nodes)
#     print_graph(graph)
