class Graph:

    def __init__(self):
        self.graph = {}

    def from_adj_matrix(self, matrix):
        self.graph = dict({i: [] for i in range(1, len(matrix) + 1)})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if value > 0 and elem != num:
                    self.graph[elem + 1].append(num + 1)

    def dfs_util(self, temp, v, visited):
        visited.add(v)
        temp.append(v)
        for neighbour in self.graph[v]:
            if neighbour not in visited:
                self.dfs_util(temp, neighbour, visited)
        return temp

    def dfs(self, v):
        visited = set()
        temp = []
        return self.dfs_util(temp, v, visited)

    def connected_components(self):
        visited = set()
        result = []
        for vertex in self.graph.keys():
            if vertex not in visited:
                temp = []
                result.append(self.dfs_util(temp, vertex, visited))
        return result


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count = int(input())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
graph = Graph()
graph.from_adj_matrix(adj_matrix)

result = graph.connected_components()
print(len(result))
for line in result:
    print(len(line), *line)


'''
4
0 1 1 0
1 0 0 0
1 0 0 0
0 0 0 0
'''