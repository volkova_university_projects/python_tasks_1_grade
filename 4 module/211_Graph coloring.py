def check(data, color, vertecies):
    for i in range(vertecies):
        for j in range(i + 1, vertecies):
            if data[i][j] and color[j] == color[i]:
                return False
    return True


def graph_coloring(data, color_count, i, color, vertecies):
    if i == vertecies:
        if check(data, color, vertecies):
            return True
        return False

    for j in range(1, color_count + 1):
        color[i] = j

        if graph_coloring(data, color_count, i + 1, color, vertecies):
            return True
        color[i] = 0
    return False


def to_list_of_ints(data):
    return list(map(int, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    vertex_count, color_count = map(int, input().split())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    color = [0 for i in range(vertex_count)]

    if graph_coloring(adj_matrix, color_count, 0, color, vertex_count):
        print('YES')
    else:
        print('NO')

'''
10 3
0 1 0 0 1 1 0 0 0 0
1 0 1 0 0 0 1 0 0 0
0 1 0 1 0 0 0 1 0 0
0 0 1 0 1 0 0 0 1 0
1 0 0 1 0 0 0 0 0 1
1 0 0 0 0 0 0 1 1 0
0 1 0 0 0 0 0 0 1 1
0 0 1 0 0 1 0 0 0 1
0 0 0 1 0 1 1 0 0 0
0 0 0 0 1 0 1 1 0 0
'''

'''
3 1
0 1 0 
0 0 1
1 0 0
'''

'''
4 5
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
'''