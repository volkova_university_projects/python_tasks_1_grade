class Graph:

    def __init__(self, matrix):
        self.adj_matrix = matrix

    def breadth_first_search(self, start, goal, parent):
        visited = [False] * len(self.adj_matrix)
        queue = [start]
        visited[start] = True
        while queue:
            u = queue.pop(0)
            for ind, val in enumerate(self.adj_matrix[u]):
                if not visited[ind] and val > 0:
                    queue.append(ind)
                    visited[ind] = True
                    parent[ind] = u
                    if ind == goal:
                        return True
        return False

    def ford_fulkerson(self, source, traffic_light):
        parent = [-1] * len(self.adj_matrix)
        max_flow = 0
        while self.breadth_first_search(source, traffic_light, parent):
            path_flow = float("Inf")
            s = traffic_light
            while s != source:
                path_flow = min(path_flow, self.adj_matrix[parent[s]][s])
                s = parent[s]
            max_flow += path_flow
            v = traffic_light
            while v != source:
                u = parent[v]
                self.adj_matrix[u][v] -= path_flow
                self.adj_matrix[v][u] += path_flow
                v = parent[v]
        return max_flow


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    traffic_light_count = int(input())
    adj_matrix = get_int_lines(traffic_light_count, row_limit=traffic_light_count)
    graph = Graph(adj_matrix)
    print(graph.ford_fulkerson(0, traffic_light_count - 1))

'''
4
0 2 0 5
2 0 3 4
0 3 0 0
5 4 0 0
'''