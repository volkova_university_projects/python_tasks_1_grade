class Graph:

    def __init__(self):
        self.graph = {}

    def from_edge_list(self, matrix):
        self.graph = dict({i: [] for i in range(1, len(matrix) + 1)})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if value > 0 and elem != num:
                    self.graph[elem + 1].append(num + 1)


    def dfs_util(self, v, visited):
        visited.add(v)
        print(v, end=' ')
        for neighbour in self.graph[v]:
            if neighbour not in visited:
                self.dfs_util(neighbour, visited)

    def dfs(self, v):
        visited = set()
        self.dfs_util(v, visited)


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count = int(input())
start_vertex = int(input())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
graph = Graph()
graph.from_edge_list(adj_matrix)

graph.dfs(start_vertex)
"""
3
1
0 1 1
0 0 0
0 0 0
"""

# Driver code


# Create a graph given
# in the above diagram
# g = Graph()
# g.add_edge(1, 2)
# g.add_edge(1, 3)
# g.add_edge(1, 2)
# g.add_edge(2, 0)
# g.add_edge(2, 3)
# g.add_edge(3, 3)
#
# print("Following is DFS from (starting from vertex 2)")
# g.dfs(1)

'''
4
0 1 1 0
1 0 0 0
1 0 0 0
0 0 0 0
1 2
1 3
'''
