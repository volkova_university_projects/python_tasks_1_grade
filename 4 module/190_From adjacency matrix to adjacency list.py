def convert_to_adj_list(matrix):
    result = {}
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value == 1:
                if result.get(i + 1):
                    result[i+1].append(j + 1)
                else:
                    result[i + 1] = [j + 1]
    return result


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count = int(input())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
adj_list = convert_to_adj_list(adj_matrix)
for vertex, adj_vertices in adj_list.items():
    print(f'{vertex}: {list_to_str(adj_vertices, separator=" ")}')
if len(adj_list) == 0:
    print()

""" 
5
0 1 1 1 0
1 0 0 1 1
1 0 0 1 0
1 1 1 0 1
0 1 0 1 0
"""

""" 
5
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0
"""