import sys


class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = [[0 for _ in range(vertices)] for _ in range(vertices)]

    def min_key(self, key, spanning_tree_set):
        min_index = 0
        minimal = sys.maxsize

        for v in range(self.V):
            if key[v] < minimal and spanning_tree_set[v] is False:
                minimal = key[v]
                min_index = v
        return min_index

    def prim_spanning_tree(self):
        key = [sys.maxsize] * self.V
        parent = [None] * self.V
        key[0] = 0
        spanning_tree_set = [False] * self.V
        parent[0] = -1
        for cout in range(self.V):
            u = self.min_key(key, spanning_tree_set)
            spanning_tree_set[u] = True
            for v in range(self.V):
                if 0 < self.graph[u][v] < key[v] and spanning_tree_set[v] is False:
                    key[v] = self.graph[u][v]
                    parent[v] = u

        return self.make_mst(parent)

    def make_mst(self, parent):
        weight_total = 0
        mst = []
        for i in range(1, self.V):
            weight_total += self.graph[i][parent[i]]
            mst.append([parent[i], i, self.graph[i][parent[i]]])
        return weight_total, mst


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def edge_list_to_adj_matrix(vertices, edges):
    adj = [[0] * vertices for _ in range(vertices)]
    result = []
    for item in edges:
        edge, vertex, weight = item
        adj[edge][vertex] = adj[vertex][edge] = weight

    for element in adj:
        result.append(" ".join(map(str, element)))
    return result


vertex_count = int(input())
g = Graph(vertex_count)
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
g.graph = adj_matrix
weight, tree = g.prim_spanning_tree()
print(weight)
print('\n'.join(edge_list_to_adj_matrix(vertex_count, tree)))


'''
7
0 0 13 19 0 0 0
0 0 20 4 15 0 0
13 20 0 0 0 0 0
19 4 0 0 0 0 17
0 15 0 0 0 22 8
0 0 0 0 22 0 10
0 0 0 17 8 10 0
'''

'''
7
[[0, 0, 13, 19, 0, 0, 0],
[0, 0, 20, 4, 15, 0, 0],
[13, 20, 0, 0, 0, 0, 0],
[19, 4, 0, 0, 0, 0, 17],
[0, 15, 0, 0, 0, 22, 8],
[0, 0, 0, 0, 22, 0, 10],
[0, 0, 0, 17, 8, 10, 0]]
'''
