from collections import namedtuple
from collections import deque

Pair = namedtuple("Pair", ["student", "family"])

def pref_to_rank(pref):
    return {
        a: {b: idx for idx, b in enumerate(a_pref)}
        for a, a_pref in pref.items()
    }

def gale_shapley(*, A, B, A_pref, B_pref):
    """Create a stable matching using the
    Gale-Shapley algorithm.

    A -- set[str].
    B -- set[str].
    A_pref -- dict[str, list[str]].
    B_pref -- dict[str, list[str]].

    Output: list of (a, b) pairs.
    """
    B_rank = pref_to_rank(B_pref)
    ask_list = {a: deque(bs) for a, bs in A_pref.items()}
    pair = {}
    #
    remaining_A = set(A)
    while len(remaining_A) > 0:
        a = remaining_A.pop()
        b = ask_list[a].popleft()
        if b not in pair:
            pair[b] = a
        else:
            a0 = pair[b]
            b_prefer_a0 = B_rank[b][a0] < B_rank[b][a]
            if b_prefer_a0:
                remaining_A.add(a)
            else:
                remaining_A.add(a0)
                pair[b] = a
    return [(a, b) for b, a in pair.items()]


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    same_gender_count = int(input())
    N = same_gender_count
    prefer = get_int_lines(same_gender_count*2)#, row_limit=same_gender_count)
    men = {index+1: preferance for index, preferance in enumerate(prefer[:same_gender_count])}
    women = {index+1: preferance for index, preferance in enumerate(prefer[same_gender_count:])}

    print(men, women, sep='\n')

    # M = dict((m, prefs.split(', ')) for [m, prefs] in (line.rstrip().split(': ')
    #                                                    for line in open('men.txt')))
    # W = dict((m, prefs.split(', ')) for [m, prefs] in (line.rstrip().split(': ')
    #                                                    for line in open('women.txt')))
    # print(M)
    match = gale_shapley(A=list(men.keys()), B=list(women.keys()), A_pref=men, B_pref=women)

    print(match)
'''
3
1 2 3
2 3 1
1 2 3
1 2 3
2 3 1
3 1 2
'''