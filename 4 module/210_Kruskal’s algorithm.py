class Graph:

    def __init__(self, vertices, edge_list):
        self.V = vertices
        self.graph = []
        self.fill_edges(edge_list)

    def add_edge(self, u, v, w):
        self.graph.append([u - 1, v - 1, w])

    def fill_edges(self, edge_list):
        for edge in edge_list:
            self.add_edge(*edge)

    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    def union(self, parent, rank, x, y):
        x_root = self.find(parent, x)
        y_root = self.find(parent, y)
        if rank[x_root] < rank[y_root]:
            parent[x_root] = y_root
        elif rank[x_root] > rank[y_root]:
            parent[y_root] = x_root
        else:
            parent[y_root] = x_root
            rank[x_root] += 1

    def kruskal_spanning_tree(self):
        result = []
        i = 0
        e = 0
        self.graph = sorted(self.graph, key=lambda item: item[2])
        parent = []
        rank = []
        for node in range(self.V):
            parent.append(node)
            rank.append(0)
        while e < self.V - 1:
            u, v, w = self.graph[i]
            i = i + 1
            x = self.find(parent, u)
            y = self.find(parent, v)

            if x != y:
                e = e + 1
                result.append([u, v, w])
                self.union(parent, rank, x, y)
        minimum_cost = 0
        for u, v, weight in result:
            minimum_cost += weight
        return minimum_cost, result


def list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def edge_list_to_adj_matrix(vertices, edges):
    adj = [[0] * vertices for _ in range(vertices)]
    result = []
    for item in edges:
        edge, vertex, weight = item
        adj[edge][vertex] = adj[vertex][edge] = weight
    for element in adj:
        result.append(" ".join(map(str, element)))
    return result


vertex_count, edge_count = list_of_ints(input().split())
edge_list = get_int_lines(edge_count, row_limit=3)
g = Graph(vertex_count, edge_list)
weight, tree = g.kruskal_spanning_tree()
print(weight)
print('\n'.join(edge_list_to_adj_matrix(vertex_count, tree)))


'''
4 8
1 2 723
2 1 723
2 3 137
2 4 470
3 2 137
3 4 69
4 2 470
4 3 69
'''

'''
1 2 723
2 1 723
2 3 137
2 4 470
3 2 137
3 4 69
4 2 470
4 2 470
'''
