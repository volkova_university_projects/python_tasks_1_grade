class Graph:

    def __init__(self):
        self.graph = {}

    def __getitem__(self, item):
        return self.graph[item]

    def from_edge_list(self, matrix):
        self.graph = dict({i: [] for i in range(1, len(matrix) + 1)})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if value > 0 and elem != num:
                    self.graph[elem + 1].append(num + 1)


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def prepare_output(data):
    if not data:
        return -1
    length = len(data) - 1
    if length:
        return f'{length}\n{list_to_str(data, separator=" ")}'
    else:
        return length


def bfs_shortest_path(graph, start, goal):
    explored = []
    queue = [[start]]

    if start == goal:
        return [0]

    while queue:
        path = queue.pop(0)
        node = path[-1]
        if node not in explored:
            neighbours = graph[node]
            for neighbour in neighbours:
                new_path = list(path)
                new_path.append(neighbour)
                queue.append(new_path)
                if neighbour == goal:
                    return new_path
            explored.append(node)

    return []


if __name__ == '__main__':
    vertex_count = int(input())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    start, end = to_list_of_ints(input().split())
    graph = Graph()
    graph.from_edge_list(adj_matrix)

    print(prepare_output(bfs_shortest_path(graph, start, end)))

'''
5
0 1 0 0 1
1 0 1 0 0
0 1 0 0 0
0 0 0 0 0
1 0 0 0 0
3 5
'''

"""    
5
0 1 0 0 1
1 0 1 0 0
0 1 0 1 1
0 0 0 0 1
1 0 1 0 0
3 5
"""

'''
5
0 1 0 0 1
1 0 1 0 0
0 1 0 0 0
0 0 0 0 0
1 0 0 0 0
4 5
'''




# def breadth_first_search(self, start, end):
#     visited = [False] * (max(self.graph) + 1)
#     queue = []
#     queue.append(start)
#     visited[start] = True
#     path = []
#     result = []
#
#     while queue:
#         vertex = queue.pop(0)
#         path.append(vertex)
#         if vertex == end:
#             result.append(path)
#             break
#         for i in self.graph[vertex]:
#             if not visited[i]:
#                 queue.append(i)
#                 visited[i] = True
#     return result