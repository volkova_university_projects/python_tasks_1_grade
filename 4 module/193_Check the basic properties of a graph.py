"""
check if graph is:
1. directed
2. weighted
3. transitive
4. complete
"""


def is_directed(matrix):
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value != matrix[j][i]:
                return 1
    return -1


def is_weighted(matrix):
    for i, row in enumerate(matrix):
        for value in row:
            if value > 1:
                return 1
    return -1


def is_transitive(matrix):
    for i, row in enumerate(matrix):
        for j, element in enumerate(row):
            if element > 0:
                for k, child in enumerate(matrix[j]):
                    if not matrix[k][i] and k != i:
                        return -1
    return 1


def is_full(matrix):
    for i, row in enumerate(matrix):
        row[i] = 1
        if 0 in row:
            return -1
    return 1


def convert_to_edge_list(matrix):
    result = []
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value > 0 and i != j:
                result.append([i + 1, j + 1])
                matrix[j][i] = 0
    return result


def convert_to_adj_list(matrix):
    result = {}
    for i, row in enumerate(matrix):
        for j, value in enumerate(row):
            if value > 0:
                if result.get(i + 1):
                    result[i + 1].append(j + 1)
                else:
                    result[i + 1] = [j + 1]
    return result


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count = int(input())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)

print(is_directed(adj_matrix))
print(is_weighted(adj_matrix))
print(is_transitive(adj_matrix))
print(is_full(adj_matrix))


"""
3
0 1 1
1 0 1
1 1 0
"""

#directed
"""
3
0 1 0
0 0 0 
0 0 0 
"""


#weighted
"""
3
0 2 0
2 0 0 
0 0 0 
"""

#transitive
"""
5
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
"""


"""
10
0 1 1 1 1 1 1 1 1 2
1 0 1 1 1 1 1 1 1 1
1 1 0 1 1 1 1 1 1 1
1 1 1 0 1 1 1 1 1 1
1 1 1 1 0 1 1 1 1 1
1 1 1 1 1 0 1 1 1 1
1 1 1 1 1 1 0 1 1 1
1 1 1 1 1 1 1 0 1 1
1 1 1 1 1 1 1 1 0 1
1 1 1 1 1 1 1 1 1 0

"""