class WeightedGraphFromEdges:

    def __init__(self, vertices, edges):
        self.graph = dict({i: {} for i in range(1, vertices + 1)})
        self.edge = []
        self._from_edge_list(edges)
        self._fill_edges()

    def __getitem__(self, item):
        return self.graph[item]

    def _from_edge_list(self, edges):
        for src, dst, weight in edges:
            self.graph[src][dst] = weight

    def _fill_edges(self):
        for src, inner in self.graph.items():
            for destination, weight in inner.items():
                self.edge.append(Edge(src, destination, weight))

    def __repr__(self):
        return f'{self.graph}'


class Edge:
    def __init__(self, src=None, dest=None, weight=None):
        self.src = src
        self.dest = dest
        self.weight = weight

    def __repr__(self):
        return f"Edge({self.src}, {self.dest}, {self.weight})"


def shortest_path_bellman_ford(graph, src):
    V = len(graph.graph)

    dist = [float('inf') for _ in range(V + 1)]
    dist[src] = 0

    for _ in range(1, V):
        for edge in graph.edge:
            u = edge.src
            v = edge.dest
            weight = edge.weight
            if (dist[u] != float('inf') and
                    dist[u] + weight < dist[v]):
                dist[v] = dist[u] + weight

    for edge in graph.edge:
        u = edge.src
        v = edge.dest
        weight = edge.weight
        if (dist[u] != float('inf') and
                dist[u] + weight < dist[v]):
            raise (Exception)

    return dist[1:]


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def replace_inf_to_no(data):
    return 'No' if data == float('inf') else data


def make_output(result):
    return f"{list_to_str(map(replace_inf_to_no, result), separator=' ')}"


if __name__ == '__main__':
    vertex_count, edge_count = to_list_of_ints(input().split())
    edges = get_int_lines(edge_count)
    graph = WeightedGraphFromEdges(vertex_count, edges)
    result = shortest_path_bellman_ford(graph, 1)
    print(make_output(result))

'''0 10 20 No No No
6 4
1 2 10
2 3 10
1 3 100
4 5 -10
'''
