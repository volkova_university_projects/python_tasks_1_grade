class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.x} {self.y}'


class Square:
    def __init__(self, A=None, B=None, C=None, D=None):
        self.A, self.B, self.C, self.D = Point(*A), Point(*B), Point(*C), Point(*D)

    def __repr__(self):
        return f'{self.A} {self.B} {self.C} {self.D}'

    @property
    def center(self):
        left_x = min(self.A.x, self.B.x, self.C.x, self.D.x)
        right_x = max(self.A.x, self.B.x, self.C.x, self.D.x)
        low_y = min(self.A.y, self.B.y, self.C.y, self.D.y)
        top_y = max(self.A.y, self.B.y, self.C.y, self.D.y)

        return Point(left_x + (right_x - left_x)/2, low_y + (top_y - low_y)/2)


def list_of_int(in_string):
    return list(map(int, in_string.split()))


if __name__ == '__main__':
    user_data = []
    for vertex in range(4):
        user_data.append(list_of_int(input()))
    square = Square(*user_data)

    print(square.center)

'''
1 1
2 4
-1 5
-2 2
'''

'''
0 3
3 0
0 0
3 3
'''
