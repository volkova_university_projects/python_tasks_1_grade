

def checker(rules, queues):
    found_rules_ok = {}
    for r_num, rule in enumerate(rules):
        for q_num, queue in enumerate(queues):
            temp_queue = ''.join(queue)
            temp_rule = ''.join(rule)
            if temp_queue.find(temp_rule) != -1:
                if not found_rules_ok.get(r_num):
                    found_rules_ok[r_num] = []
                found_rules_ok[r_num].append(q_num)
    return found_rules_ok


def make_substrings(rule):
    rule_shorts = []
    for i in range(len(rule)):
        if i < len(rule) - 1:
            rule_shorts.append(rule[i:i + 2])
    return rule_shorts


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def get_str_lines(lines, *, row_limit=None):
    return [input().split()[:row_limit] for _ in range(lines)]


if __name__ == '__main__':
    rules_count, queue_count = to_list_of_ints(input().split())
    rules = get_str_lines(rules_count)
    queues = get_str_lines(queue_count)
    found_rules_ok = checker(rules, queues)
    print(found_rules_ok)
    found_subrules_fail = False
    for r_num, rule in enumerate(rules):
        subrules = []
        for sub_rule in make_substrings(rule):
            subrules.append(sub_rule)
        print(subrules)
        r_queues = []
        for q_num, queue in enumerate(queues):
            if r_num not in found_rules_ok or not q_num in found_rules_ok[r_num]:
                r_queues.append(queue)
            else:
                temp_queue = "".join(queue)
                temp_rule = "".join(rule)
                q = temp_queue.replace(temp_rule, '')
                r_queues.append(list(q))
        print(r_queues)
        fail = len(checker(subrules, r_queues))
        if fail > 0:
            found_subrules_fail = True
            break
    if found_subrules_fail:
        print('EPIC FAIL')
    elif not found_rules_ok:
        print('EPIC FAIL')
    else:
        print('NO')




'''
3 1
a d c f
a b d e
e f h d
z s a b d e f h d
'''

'''
4 1
a d с f
a b d e
e f h d
d a c z
z s a b d e f h d

'''

'''
2 1 
a b c 
d e f
d e f

'''

'''
2 1
d e f
d e f
d e f
'''

'''
2 1
a b c
d e f
d e f
'''

'''
2 1
d e f
d e f
d e f
'''

'''
2 2
d e f
f d e
d e f
z s a b d e f h d
'''