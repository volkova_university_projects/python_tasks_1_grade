class Graph:

    def __init__(self):
        self.graph = {}

    def from_edge_list(self, matrix):
        self.graph = dict({i: [] for i in range(1, len(matrix) + 1)})
        for elem, row in enumerate(matrix):
            for num, value in enumerate(row):
                if value > 0 and elem != num:
                    self.graph[elem + 1].append(num + 1)

    def dfs_util(self, v, visited):
        visited.add(v)
        print(v, end=' ')
        for neighbour in self.graph[v]:
            if neighbour not in visited:
                self.dfs_util(neighbour, visited)

    def dfs(self, v):
        visited = set()
        self.dfs_util(v, visited)


def get_dict(array):
    result = []


def to_list_of_ints(data):
    return list(map(int, data))


def get_int_lines(lights, *, row_limit=None):
    return [to_list_of_ints(input().split()) for _ in range(lights)]


lights_count = int(input())
lights_matrix = get_int_lines(lights_count)
print(lights_matrix)


'''
5
1 2 3
2 1 4
3 1 
4 1 
5
'''
# graph = Graph()
# graph.from_edge_list(adj_matrix)
#
# graph.dfs(start_vertex)