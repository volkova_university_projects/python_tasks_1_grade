def comparison(first, second):
    if len(first) > len(second):
        return first
    elif len(second) > len(first):
        return second
    elif len(first) == len(second):
        for i in range(len(first)):
            num, elem = first[i], second[i]
            if num == elem:
                continue
            return first if num > elem else second


def join(result):
    res = ''.join(map(str, result))
    return res


first, second = input().split()
if int(first) <= 0 or int(second) <= 0:
    print(0)
    exit()
first, second = list(map(int, first)), list(map(int, second))
print(join(comparison(first, second)))