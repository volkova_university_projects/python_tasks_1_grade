number = int(input())
raw_numbers = list(map(int, input().split()))
all_numbers = raw_numbers[:number]
index, value = list(map(int, input().split()))
all_numbers.append(None)
length = len(all_numbers)

for num in range(length):
    if length - num > index:
        all_numbers[length - num - 1] = all_numbers[length - num - 2]
    if length - num == index:
        all_numbers[length - num - 1] = value
        break
print(*all_numbers)