number = int(input())
raw_numbers = list(map(int, (input().split())))
all_numbers = raw_numbers[:number]
i = 1
evens = []
for i in all_numbers:
    if i % 2 != 0:
        continue
    if i % 2 == 0:
        evens.append(i)
print(*evens)
