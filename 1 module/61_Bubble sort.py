def bubbleSort(array):
    length = len(array)
    for elem in range(length - 1):
        for num in range(0, length - elem - 1):
            if array[num] > array[num + 1]:
                array[num], array[num + 1] = array[num + 1], array[num]
    return array


raw_numbers = list(map(int, input().split()))
length = raw_numbers[0]
array = raw_numbers[1:length + 1]
print(*bubbleSort(array))