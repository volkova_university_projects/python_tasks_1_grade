import math, cmath


def solver(a, b, c):
    if abs(a) > 1000 or abs(b) > 1000 or abs(c) > 1000:
        return -1
    elif a == 0 and b == 0 and c == 0:
        return -1
    elif b == 0 and c == 0:
        return '1 0'
    elif a == 0 and b and c:
        return f'1 {b / (-1 * c):.6g}'
    elif b == 0:
        if (-c / a) > 0:
            x1 = math.sqrt(-c / a)
            x2 = - math.sqrt(-c / a)
            if x1 < x2:
                return f'2 {x1:.6g} {x2:.6g}'
            else:
                return f'2 {x2:.6g} {x1:.6g}'
        else:
            return -1
    elif c == 0:
        if a == 0:
            return -1
        else:
            x1 = 0
            x2 = -b / a
            return f'2 {x1:.6g} {x2:.6g}'
    D = b*b - 4*a*c
    if D > 0:
        x1 = (-b + math.sqrt(D)) / (2*a)
        x2 = (-b - math.sqrt(D)) / (2*a)
        if x1 == x2:
            text = f'1 {x1:.6g}'
        if x1 > x2:
            text = f'2 {x2:.6g} {x1:.6g}'
        if x2 > x1:
            text = f'2 {x1:.6g} {x2:.6g}'
    elif D == 0:
        x = -(0.5 * b) / a
        text = f'1 {x:.6g}'
    else:
        x1 = (-b - cmath.sqrt(D))/(2 * a)
        x2 = (-b + cmath.sqrt(D))/(2 * a)
        if x1 == x2:
            text = f'1 {x1:.6g}'
        else:
            text = f'2 {x1:.6g} {x2:.6g}'
    return text

a = list(map(int, input().split()))
a, b, c = a[0], a[1], a[2]

print(solver(a, b, c))
exit(0)
discr = b**2 - 4 * a * c

if abs(a) > 1000 or abs(b) > 1000 or abs(c) > 1000:
    print(-1)
elif a == 0 and b == 0 and c == 0:
    print(-1)
elif b == 0 and c == 0:
    print(1, 0)
elif a == 0:
    print(f'1 {b/c:.6g}')
elif b == 0:
    if (-c / a) > 0:
        x1 = math.sqrt(-c / a)
        x2 = - math.sqrt(-c / a)
        if x1 < x2:
            print(2, int(x1), int(x2))
        else:
            print(2, int(x2), int(x1))
    else:
        print(-1)
elif c == 0:
    if a == 0:
        print(-1)
    else:
        x1 = 0
        x2 = -b / a
        print(2, int(x1), int(x2))
elif discr == 0:
    x = -(0.5 * b) / a
    print(1, int(x))
elif discr > 0:
    x1 = (-b + math.sqrt(discr)) / 2 * a
    x2 = (-b - math.sqrt(discr)) / 2 * a
    if x1 < x2:
        print(2, int(x1), int(x2))
    else:
        print(2, int(x2), int(x1))
else:
    print(-1)




# import math
#
# print("Введите коэффициенты для уравнения")
# print("ax^2 + bx + c = 0:")
# a = float(input("a = "))
# b = float(input("b = "))
# c = float(input("c = "))
#
# discr = b ** 2 - 4 * a * c
# print("Дискриминант D = %.2f" % discr)
#
# if discr > 0:
#     x1 = (-b + math.sqrt(discr)) / (2 * a)
#     x2 = (-b - math.sqrt(discr)) / (2 * a)
#     if x1 > x2:
#         print(2, x1, x2)
#     else:
#         print(2, x2, x1)
#     print("x1 = %.2f \nx2 = %.2f" % (x1, x2))
# elif discr == 0:
#     x = -b / (2 * a)
#     print(1, x)
#     print("x = %.2f" % x)
# else:
#     print("Корней нет")

# discr = b**2 - 4 * a * c
# print((discr))
# if discr > 0:
#     x1 = (-b + math.sqrt(discr)) / (2 * a)
#     x2 = (-b - math.sqrt(discr)) / (2 * a)
#     print(x1, x2)
# elif discr == 0:
#     x = -b / (2 * a)
#     print(x)
# else:
#     print("Корней нет")


# a = list(map(int, input().split()))
# a, b, c = a[0], a[1] if len(a) > 1 else 0, a[2] if len(a) > 2 else 0
# discr = b**2 - 4 * a * c
# if discr < 0:
#     print(-1)
# elif discr == 0:
#     x = -b / (2 * a)
#     print(1, int(x))
# else:
#     x1 = (-b + discr**0.5) / (2 * a)
#     x2 = (-b - discr** 0.5) / (2 * a)
#     if x1 > x2:
#         print(2, int(x2), int(x1))
#     else:
#         print(2, int(x1), int(x2))


# import math, cmath
#
#
# def solver(a, b, c):
#     if abs(a) > 1000 or abs(b) > 1000 or abs(c) > 1000:
#         return -1
#     elif a == 0 and b == 0 and c == 0:
#         return -1
#     elif b == 0 and c == 0:
#         return '1 0'
#     elif a == 0 and b and c:
#         return f'1 {b / (-1 * c):.6g}'
#     elif b == 0:
#         if (-c / a) > 0:
#             x1 = math.sqrt(-c / a)
#             x2 = - math.sqrt(-c / a)
#             if x1 < x2:
#                 return f'2 {x1:.6g} {x2:.6g}'
#             else:
#                 return f'2 {x2:.6g} {x1:.6g}'
#         else:
#             return -1
#     elif c == 0:
#         if a == 0:
#             return -1
#         else:
#             x1 = 0
#             x2 = -b / a
#             return f'2 {x1:.6g} {x2:.6g}'
#     D = b*b - 4*a*c
#     if D > 0:
#         x1 = (-b + math.sqrt(D)) / (2*a)
#         x2 = (-b - math.sqrt(D)) / (2*a)
#         if x1 == x2:
#             text = f'1 {x1:.6g}'
#         if x1 > x2:
#             text = f'2 {x2:.6g} {x1:.6g}'
#         if x2 > x1:
#             text = f'2 {x1:.6g} {x2:.6g}'
#     elif D == 0:
#         x = -(0.5 * b) / a
#         text = f'1 {x:.6g}'
#     else:
#         x1 = (-b - cmath.sqrt(D))/(2 * a)
#         x2 = (-b + cmath.sqrt(D))/(2 * a)
#         if x1 == x2:
#             text = f'1 {x1:.6g}'
#         else:
#             text = f'2 {x1:.6g} {x2:.6g}'
#     return text
#
# a = list(map(int, input().split()))
# a, b, c = a[0], a[1], a[2]
#
# print(solver(a, b, c))