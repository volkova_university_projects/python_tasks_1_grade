def bubble_sort(all_numbers):
    length = len(all_numbers)
    for elem in range(length - 1):
        for num in range(0, length - elem - 1):
            if all_numbers[num] > all_numbers[num + 1]:
                all_numbers[num], all_numbers[num + 1] = all_numbers[num + 1], all_numbers[num]
    return all_numbers


def uniq(all_numbers):
    unique = []
    for num in all_numbers:
        if num not in unique:
            unique.append(num)
    return unique


number = input()
all_numbers = list(map(int, number))
print(*(bubble_sort(uniq(all_numbers))))