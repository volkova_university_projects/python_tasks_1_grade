length = int(input())
raw_numbers = list(map(int, input().split()))
numbers = raw_numbers[0:length]
result = numbers.copy()
for i in range(length):
    result[length - i - 1] = numbers[i]
print(*result)
