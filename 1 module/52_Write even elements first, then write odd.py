number = int(input())
raw_numbers = list(map(int, (input().split())))
all_numbers = raw_numbers[:number]
# print(all_numbers)
odds, evens = [], []
for i in all_numbers:
    if i % 2 == 0 and i != 0:
        evens.append(i)
    if i % 2 != 0 and i != 0:
        odds.append(i)
print(*evens, *odds)