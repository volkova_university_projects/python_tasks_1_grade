number = int(input())
if number % 2 != 0:
    print('No')
elif number % 2 == 0:
    count = 0
    while 2**count <= number:
        count = count + 1
        if 2**count == number:
            print('Yes')
            break
    else:
        print('No')