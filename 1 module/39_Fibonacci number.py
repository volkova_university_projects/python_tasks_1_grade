def iterative_fib(number):
    num_1, num_2 = 0, 1

    for i in range(1, number):
        num_1, num_2 = num_2, num_1 + num_2

    return num_1

number = int(input())
print(iterative_fib(number))