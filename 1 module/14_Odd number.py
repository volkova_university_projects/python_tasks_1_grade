a = list(map(int, input().split()))
a, b = a[0], a[1]

if a % 2 != 0:
    print(a)
else:
    print(b)