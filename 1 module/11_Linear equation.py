a = list(map(int, input().split()))
a, b = a[0], a[1]

if a == 0 and b == 0:
    print('R')
elif a == 0 and b != 0:
    print('NO SOLUTION')
else:
    print(-b // a)

