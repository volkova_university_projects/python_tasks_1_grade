number = int(input())
total = 1
while total <= number:
    total = total + 1
    if number % total == 0:
        print(total)
        break
else:
    print(-1)