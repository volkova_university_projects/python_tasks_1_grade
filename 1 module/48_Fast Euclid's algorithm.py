raw_numbers = list(map(int, input().split()))
first, second = raw_numbers

while first != 0 and second != 0:
    if first > second:
        first = first % second
    else:
        second = second % first

print(first + second)