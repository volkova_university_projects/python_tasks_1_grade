number = int(input())
if number == 0:
    print(1)
else:
    fib_prev, fib_next = 0, 1
    n = 1
    while fib_next <= number:
        if fib_next == number:
            print(n+1)
            break
        fib_prev, fib_next = fib_next, fib_prev + fib_next
        n += 1
    else:
        print(0)