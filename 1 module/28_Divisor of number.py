number = int(input())
x = 1
count = []
while x**2 <= number:
    if number % x == 0:
        count.append(x)
        if x != number // x:
            count.append(number // x)
    x += 1
print(len(count))