def longest_increasing_subsequence(array):
    sub = [None] * len(array)
    previous = sub.copy()
    result = 1
    sub[0] = 0

    for i in range(1, len(array)):
        lower = 0
        upper = result
        if array[sub[upper - 1]] < array[i]:
            j = upper
        else:
            while upper - lower > 1:
                mid = (upper + lower) // 2
                if array[sub[mid - 1]] < array[i]:
                    lower = mid
                else:
                    upper = mid
            j = lower
        previous[i] = sub[j-1]
        if j == result or array[i] < array[sub[j]]:
            sub[j] = i
            result = max(result, j + 1)
    return result if result > 1 else -1


length = int(input())
data = list(map(int, input().split()))
print(longest_increasing_subsequence(data))