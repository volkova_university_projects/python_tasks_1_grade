number = int(input())
x = 2
while x * x <= number:
    if number % x == 0:
       print(number // x)
       break
    x += 1
else:
    print(-1)