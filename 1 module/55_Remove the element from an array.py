number = int(input())
raw_numbers = list(map(int, input().split()))
all_numbers = raw_numbers[:number]
index = int(input())
length = len(all_numbers)
for num in range(index - 1, length):
    if num < length - 1:
        all_numbers[num] = all_numbers[num + 1]
    else:
        del(all_numbers[num])
print(*all_numbers)