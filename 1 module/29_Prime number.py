number = int(input())
result = []
for divisor in range(1, (number + 1) // 2):
    if number % divisor == 0:
        result.append(divisor)
is_prime = 'Yes' if len(result) == 1 else 'No'
print(is_prime)

# #include <iostream>
# #include <vector>
# using namespace std;
#
# int main()
# {
#     int number;
#     vector<int> result(0);
#     std::cin >> number;
#     for (int i = 1; i <= (number / 2); i++)
#     {
#         if (number % i == 0) {
#             result.push_back(i);
#         }
#     }
#
#     if (result.size() == 1) {
#         std::cout << "Yes";
#     } else std::cout << "No";
# }
