def counting(data):
    counts = [0 for i in range(max(data) + 1)]

    for value in data:
        counts[value] += 1

    for index in range(1, len(counts)):
        counts[index] = counts[index - 1] + counts[index]

    all_numbers = [0 for i in range(len(data))]
    for value in data:
        index = counts[value] - 1
        all_numbers[index] = value
        counts[value] -= 1

    return all_numbers


raw_numbers = list(map(int, input().split()))
limit = raw_numbers[0]
if limit <= 0:
    print(0)
    exit()
all_numbers = raw_numbers[1:limit + 1]
largest = len(all_numbers)
for i in range(len(all_numbers)):
    if all_numbers[i] < 0:
        print(0)
        exit()
# print(limit, all_numbers)
print(*counting(all_numbers))