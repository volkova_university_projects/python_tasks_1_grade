# # swap to variables without a third
# given = list(input().split())
# print(given)
# a, b = given[0], given[1]
# a, b = b, a
# print(a, b)

# a = int(input('Введите первое значение: '))
# b = int(input('Введите второе значение: '))
given = list(input().split())
a, b = int(given[0]), int(given[1])
a = a + b
b = a - b
a = a - b

print(a, b)