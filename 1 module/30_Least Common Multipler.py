def gcd(first_number, second_number):
    while (second_number > 0):
        first_number, second_number = second_number, first_number % second_number
    return first_number


def lcm(first_number, second_number):
    return (first_number * second_number) // gcd(first_number, second_number)


first_number, second_number = map(int, input().split())

print(lcm(first_number, second_number))