# import sys
#
# number = int(input())
# raw_numbers = sys.stdin.readline().split()
# all_numbers = raw_numbers[:number]
# unique = []
# for num in all_numbers:
#     if num not in unique:
#         unique.append(num)
#         sys.stdout.write(f'{num} ')
#
# sys.stdout.flush()


def uniq(all_numbers):
    unique = {i: 0 for i in all_numbers}
    for num in all_numbers:
        if unique[num] == 0:
            print(num, end=" ")
            unique[num] = 1


number = int(input())
raw_numbers = input().split()
all_numbers = raw_numbers[:number]
uniq(all_numbers)


