ascii_dict = dict()
ascii_in_number = range(0, 256)
for i in ascii_in_number:
    ascii_dict[str(i)] = chr(i)

search = input()
for key, srch in ascii_dict.items():
    if key == search:
        print(srch)
