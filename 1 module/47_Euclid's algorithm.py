raw_numbers = list(map(int, input().split()))
first, second = raw_numbers

while first != second:
    if first > second:
        first = first - second
    else:
        second = second - first

print(first)