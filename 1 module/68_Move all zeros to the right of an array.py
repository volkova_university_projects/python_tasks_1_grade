def zero_in_end(all_numbers):
    i = 0
    limit = len(all_numbers)
    while i < limit:
        if all_numbers[i] == 0:
            for j in range(i, limit - 1):
                all_numbers[j] = all_numbers[j + 1]
            all_numbers[limit - 1] = 0
            limit -= 1
        else:
            i += 1
    return all_numbers

number = int(input())
raw_numbers = list(map(int, input().split()))
all_numbers = raw_numbers[:number]

print(*zero_in_end(all_numbers))