
def match(all_numbers):
    for elem in range(len(all_numbers) - 1):
        for num in range(elem + 1, len(all_numbers)):
            if all_numbers[elem] == all_numbers[num]:
                return all_numbers[elem]


number = int(input())
raw_numbers = list(map(int, input().split()))
all_numbers = raw_numbers[:number]
print(match(all_numbers))
