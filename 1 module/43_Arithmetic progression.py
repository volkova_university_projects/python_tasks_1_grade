def is_arithmetic(numbers):
    delta = numbers[1] - numbers[0]
    for index in range(len(numbers) - 1):
        if not (numbers[index + 1] - numbers[index] == delta):
            return 0
    return 1


raw_numbers = list(map(int, input().split()))
num = raw_numbers[0:1]
numbers = raw_numbers[1:num[0] + 1]
print(is_arithmetic(numbers))