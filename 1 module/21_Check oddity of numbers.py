n = list(map(int, input().split()))
if n[0] % 2 != 0 and n[1] % 2 != 0:
    print(0)
elif n[0] % 2 == 0 and n[1] % 2 != 0:
    print(1)
elif n[0] % 2 != 0 and n[1] % 2 == 0:
    print(1)
else:
    print(0)