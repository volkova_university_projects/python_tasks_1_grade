def all_primes(number):
    primes = 2
    total = 0
    bool_off = True
    while bool_off:
        condition = True
        j = 2
        while condition and j < primes:
            if not primes % j:
                condition = False
            j += 1
        if condition:
            print(primes, end=' ')
            total += 1
        primes += 1
        if total >= number:
            bool_off = False

number = int(input())
all_primes(number)