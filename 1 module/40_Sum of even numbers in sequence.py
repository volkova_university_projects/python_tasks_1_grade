raw_numbers = list(map(int, input().split()))
num = raw_numbers[0:1]
numbers = raw_numbers[1:num[0] + 1]

sum = 0

for elem in numbers:
    if elem % 2 == 0:
        sum += elem
print(sum)