def lex_sort(line_1, line_2):
    x, y = len(line_1), len(line_2)
    short_string_len = min(x, y)

    for i in range(short_string_len):
        if ord(line_1[i]) == ord(line_2[i]):
            continue
        elif ord(line_1[i]) < ord(line_2[i]):
            return line_1
        elif ord(line_1[i]) > ord(line_2[i]):
            return line_2
    else:
        if x <= y:
            return line_1
        else:
            return line_2


if __name__ == "__main__":
    line_1, line_2 = input(), input()
    result = lex_sort(line_1, line_2)
    print(result)

