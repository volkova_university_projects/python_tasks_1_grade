
def min_odd(x):
    odds = []
    for elem in x:
        if elem % 2 != 0:
            odds.append(elem)
    if odds:
        return min(odds)
    else:
        return -1


def max_even(x):
    evens = []
    for elem in x:
        if elem % 2 == 0:
            evens.append(elem)
    if evens:
        return max(evens)
    else:
        return -1

number = int(input())
raw_numbers = list(map(int, input().split()))
all_numbers = raw_numbers[:number]
print(min_odd(all_numbers), max_even(all_numbers))