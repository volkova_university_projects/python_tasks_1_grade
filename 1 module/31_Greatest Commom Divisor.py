first, second = map(int, input().split())
while max(first, second) % min(first, second) != 0:
    if first > second:
        first = first % second
    elif first < second:
        second = second % first
print(min(first, second))

