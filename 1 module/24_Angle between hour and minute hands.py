hours, minutes = map(int, input().split())
angle = abs(hours * 30 - minutes * 6)
angle = angle if angle < 360-angle else 360 - angle
print(angle)