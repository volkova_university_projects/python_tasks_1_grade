

def to_system(number, base):
    alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    b = alphabet[number % base]
    while number >= base:
        number = number // base
        b += alphabet[number % base]
    return b[::-1]


number, base_in, base_out = input().split()
base_in, base_out = int(base_in), int(base_out)

final_number = int(number, base_in)
final_number = to_system(final_number, base_out)
print(final_number)