
def bubbleSort(array):
    length = len(array)
    for elem in range(length - 1):
        for num in range(0, length - elem - 1):
            if array[num] > array[num + 1]:
                array[num], array[num + 1] = array[num + 1], array[num]
    return array


two_numbers = list(map(int, input().split()))
first_string, second_string = two_numbers

first_raw_numbers = list(map(int, input().split()))
second_raw_numbers = list(map(int, input().split()))

first_numbers = first_raw_numbers[:first_string]
second_numbers = second_raw_numbers[:second_string]

all_numbers = first_numbers + second_numbers
print(*bubbleSort(all_numbers))
