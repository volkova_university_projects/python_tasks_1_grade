def printknapSack(W, wt, val, n):
    K = [[0 for w in range(W + 1)]
         for i in range(n + 1)]

    # Build table K[][] in bottom
    # up manner
    for i in range(n + 1):
        for w in range(W + 1):
            if i == 0 or w == 0:
                K[i][w] = 0
            elif wt[i - 1] <= w:
                K[i][w] = max(val[i - 1]
                              + K[i - 1][w - wt[i - 1]],
                              K[i - 1][w])
            else:
                K[i][w] = K[i - 1][w]

    # stores the result of Knapsack
    res = K[n][W]
    print(res)

    w = W
    for i in range(n, 0, -1):
        if res <= 0:
            break
        if res == K[i - 1][w]:
            continue
        else:
            print(i)
            res = res - val[i - 1]
            w = w - wt[i - 1]


# # Driver code
# value_raw = [1000, 550, 550, 550, 550]
# weights_raw = [80, 50, 50, 50, 50]
# capacity = 100
# n = len(val)
#
# printknapSack(capacity, weights_raw, value_raw, len(weights_raw))

if __name__ == '__main__':
    quantity, capacity = list(map(int, input().split()))

    if 1 <= quantity >= 1000:
        raise AssertionError

    value_raw = list(map(int, input().split()))
    weights_raw = list(map(int, input().split()))

    print(printknapSack(capacity, weights_raw, value_raw, len(weights_raw)))
# # Driver code
# val = [60, 100, 120]
# wt = [10, 20, 30]
# W = 50
# n = len(val)
#
# printknapSack(W, wt, val, n)

'''
10 100
0 0 0 0 0 0 50 50 50 50
0 0 0 100 0 0 0 0 0 100
'''
'''
5 100
1000 550 550 550 550
80 50 50 50 50
'''