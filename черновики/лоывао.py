import sys
from queue import Queue


class Node:
    color = 1
    edges = set()


def can_paint(nodes, n, color_count):
    visited = [0 for _ in range(n + 1)]
    max_colors = 1
    for i in range(1, n + 1):
        if visited[i]:
            continue
        visited[i] = 1
        q = Queue()
        q.put(i)
        while not q.empty():
            top = q.get()
            for j in nodes[top].edges:
                if nodes[top].color == nodes[j].color:
                    nodes[j].color += 1
                max_colors = max(max_colors, max(
                    nodes[top].color, nodes[j].color))
                if max_colors > color_count:
                    return 'NO'
                if not visited[j]:
                    visited[j] = 1
                    q.put(j)

    return 'YES'


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(sys.stdin.readline().split())[:row_limit] for _ in range(lines)]


if __name__ == "__main__":
    vertex_count, color_count = to_list_of_ints(input().split())
    graph = get_int_lines(vertex_count)

    nodes = []
    for _ in range(vertex_count + 1):
        nodes.append(Node())

    for i in range(vertex_count):
        for j in range(vertex_count):
            if graph[i][j]:
                nodes[i].edges.add(i)
                nodes[j].edges.add(j)

    print(can_paint(nodes, vertex_count, color_count))