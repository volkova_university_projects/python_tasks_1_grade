

def knapSack(capacity, weights, value, quantity):
    if quantity == 0 or capacity == 0:
        return 0

    if (weights[quantity - 1] > capacity):
        return knapSack(capacity, weights, value, quantity - 1)

    else:
        return max(value[quantity - 1] + knapSack(capacity - weights[quantity - 1], weights, value, quantity - 1),
                   knapSack(capacity, weights, value, quantity - 1))



ingots_and_capacity = list(map(int, input().split()))
quantity, capacity = ingots_and_capacity[0], ingots_and_capacity[1]

if 1 <= quantity >= 1000:
    raise AssertionError

value_raw = list(map(int, input().split()))
value = value_raw[:quantity]
weights_raw = list(map(int, input().split()))
weights = weights_raw[:quantity]

print(knapSack(capacity, weights, value, quantity))

'''
5 100
1000 550 550 550 550
80 50 50 50 50
'''
'''
11 100
1000 550 0 9999 0 -1111 0 0 0 0 550
80 50 0 0 0 0 0 0 0 0 50
'''
'''
11 100
0 0 0 0 0 0 -1 0 0 0 0
0 0 0 0 0 0 100 0 0 0 0
'''
