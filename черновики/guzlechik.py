

def get_max_trava(trava):
    max_trava = []
    for i in range(len(trava)):
        previous_max_trava = []
        if i >= 3:
            previous_max_trava.append(max_trava[i - 3])
        if i >= 2:
            previous_max_trava.append(max_trava[i - 2])

        max_trava_in_position = trava[i]

        if len(previous_max_trava) > 0:
            max_trava_in_position += max(previous_max_trava)
        max_trava_in_position = max(max_trava_in_position, 0)
        max_trava.append(max_trava_in_position)

    return max_trava[-1]

water_lily = int(input())
if 0 < water_lily > 10**5:
    raise AssertionError
count_lily_raw = list(map(int, input().split()))
count_water_lily = count_lily_raw[0:water_lily]

print(get_max_trava(count_water_lily))

'''
5
3 -3 6 1 5

6
1 4 5 10 20 6
'''


# print(get_max_trava([3, -3, 6, 1, 5]))
# print(get_max_trava([-50, 4, 5, 10, 20, 6]))


# num_kuvshinok = input()
# kuvshinki = list(map(int, input().split()))
