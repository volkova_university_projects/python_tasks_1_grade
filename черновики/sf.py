

def checker(rules, queues):
    found_rules_ok = {}
    for r_num, rule in enumerate(rules):
        for q_num, queue in enumerate(queues):
            temp_queue = ''.join(queue)
            temp_rule = ''.join(rule)
            if temp_queue.find(temp_rule) == -1:
                if not found_rules_ok.get(r_num):
                    found_rules_ok[r_num].append(q_num)
    return found_rules_ok


def make_substring(rule):
    rule_shorts = []
    for i in range(len(rule)):
        if i < len(rule) - 1:
            rule_shorts.append(rule[i:i + 2])
    return rule_shorts


def to_list_of_ints(data):
    return list(map(int, data))


def list_to_str(data, *, separator=""):
    return separator.join(map(str, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


def get_str_lines(lines, *, row_limit=None):
    return [input().split()[:row_limit] for _ in range(lines)]



if __name__ == '__main__':
    rule_count, queue_count = to_list_of_ints(input().split())
    rules = get_str_lines(rule_count)
    queues = get_str_lines(queue_count)
    print(rules)
    print(queues)
    found_rules_ok, found_rules_fail, not_found_rules = checker(rules, queues)

    print('found rules')
    for item in found_rules_ok.items():
        print(item)
    # print(checker(rules, queues))
    #
    # for item in not_found_rules:
    #     print('not found rules')
    #     print(item)
    #
    # for item in found_rules_ok:
    #     print('found rules')
    #     print(item)

    subrules = []
    for rule in rules:
        for sub_rule in make_substring(rule):
            subrules.append(sub_rule)

        r_queues = []
        found_subrules = checker(subrules, queues)


        print('found subrules')
        for item in found_subrules:
            print(item)




'''
3 1
a d c f
a b d e
e f h d
z s a b d e f h d
'''

'''
4 1 
a d c f
e f h d
e f h d
d a c z
z s a b d e f h d
'''

'''
2 1 
a b c 
d e f
d e f
'''

'''
2 1
d e f
d e f
d e f
'''