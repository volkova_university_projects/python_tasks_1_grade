from queue import Queue


class Node:
    color = 1
    edges = set()


def can_paint(data, vertices, chromatic):
    visited = [0 for _ in range(vertices + 1)]
    max_colors = 1

    for _ in range(1, vertices + 1):
        if visited[_]:
            continue
        visited[_] = 1
        q = Queue()
        q.put(_)

        while not q.empty():
            top = q.get()
            for i in data[top].edges:
                if data[top].color == data[i].color:
                    data[i].color += 1
                max_colors = max(max_colors, max(
                    data[top].color, data[i].color))

                if max_colors > chromatic:
                    return max_colors
                if not visited[i]:
                    visited[i] = 1
                    q.put(i)
    return 'YES'


def to_list_of_ints(data):
    return list(map(int, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


if __name__ == "__main__":
    vertex_count, color_count = map(int, input().split())
    adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)
    color = [0 for i in range(vertex_count)]
    nodes = []

    for elem in range(vertex_count + 1):
        nodes.append(Node())
    for elem in range(vertex_count):
        for x in range(vertex_count):
            if adj_matrix[elem][x]:
                nodes[elem].edges.add(elem)
                nodes[x].edges.add(x)

    print(can_paint(nodes, vertex_count, color_count))

'''
10 3
0 1 0 0 1 1 0 0 0 0
1 0 1 0 0 0 1 0 0 0
0 1 0 1 0 0 0 1 0 0
0 0 1 0 1 0 0 0 1 0
1 0 0 1 0 0 0 0 0 1
1 0 0 0 0 0 0 1 1 0
0 1 0 0 0 0 0 0 1 1
0 0 1 0 0 1 0 0 0 1
0 0 0 1 0 1 1 0 0 0
0 0 0 0 1 0 1 1 0 0
'''

'''
3 1
0 1 0 
0 0 1
1 0 0
'''

'''
4 1
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
'''