from черновики.ex import knapSack
from unittest import TestCase


class Test_knapsack(TestCase):

    def test_knap_sack_1(self):
        slots, capacity = list_of_int("5 100")
        values = list_of_int("1000 550 550 550 550")
        weights = list_of_int("80 50 50 50 50")
        expected = 1100
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)

    def test_knap_sack_2(self):
        slots, capacity = list_of_int("10 100")
        values = list_of_int("1000 50 50 50 50 50 50 50 50 50")
        weights = list_of_int("100 0 0 0 0 0 0 0 0 100 ")
        expected = 1400
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)

    def test_knap_sack_3(self):
        slots, capacity = list_of_int("1 100")
        values = list_of_int("1000")
        weights = list_of_int("1000")
        expected = 0
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)

    def test_knap_sack_4(self):
        slots, capacity = list_of_int("10 100")
        values = list_of_int("1000 -50 50 50 50 50 50 50 50 50")
        weights = list_of_int("100 0 0 0 0 0 0 0 0 100 ")
        expected = 1350
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)

    def test_knap_sack_5(self):
        slots, capacity = list_of_int("10 100")
        values = list_of_int("1000 -50 50 50 50 50 50 50 50 50")
        weights = list_of_int("100 0 1 0 0 0 0 0 0 100 ")
        expected = 1300
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)

    def test_knap_sack_6(self):
        slots, capacity = list_of_int("10 100")
        values = list_of_int("0 0 0 0 0 0 50 50 50 50")
        weights = list_of_int("0 0 0 100 0 0 0 0 0 100 ")
        expected = 200
        self.assertEqual(knapSack(capacity, weights, values, slots), expected)


def list_of_int(in_string):
    return list(map(int, in_string.split()))

