class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = [[0 for column in range(vertices)] for row in range(vertices)]

    def check(self, v, colour, c):
        for i in range(self.V):
            if self.graph[v][i] == 1 and colour[i] == c:
                return False
        return True

    def graph_colour_util(self, m, colour, v):
        if v == self.V:
            return True

        for c in range(1, m + 1):
            if self.check(v, colour, c):
                colour[v] = c
                if self.graph_colour_util(m, colour, v + 1):
                    return True
                colour[v] = 0

    def graph_colouring(self, m):
        colour = [0] * self.V
        if self.graph_colour_util(m, colour, 0) is None:
            return False
        return True


def to_list_of_ints(data):
    return list(map(int, data))


def get_int_lines(lines, *, row_limit=None):
    return [to_list_of_ints(input().split())[:row_limit] for _ in range(lines)]


vertex_count, color_count = map(int, input().split())
adj_matrix = get_int_lines(vertex_count, row_limit=vertex_count)

g = Graph(vertex_count)
g.graph = adj_matrix
print('YES' if g.graph_colouring(color_count) else 'NO')


